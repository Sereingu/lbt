# Toutes les fonctions sont fonctionnelles et interchangeables, exceptés pour l'import (obligatoire en premier)
# Des classes ont été ajoutées : FusionColonne, et les classes du sous-package changements

#imports des packages et classes :
import numpy

from ptd.model.table import Table
from ptd.graphiques import Graphiques

from ptd.importation.importJsongz import ImportJsongz
from ptd.importation.importJson import ImportJson
from ptd.importation.importCsvgz import ImportCsvgz
from ptd.importation.importCsv import ImportCsv

from ptd.transform.changements.conversion_type import ConversionType
from ptd.transform.changements.renommer import Renommer
from ptd.transform.changements.mise_a_jour import MiseAJour
from ptd.transform.changements.ajouter_colonne import AjouterColonne
from ptd.transform.changements.ajouter_ligne import AjouterLigne
from ptd.transform.changements.supprimer_colonne import SupprimmerColonne
from ptd.transform.changements.supprimer_ligne import SupprimmerLigne

from ptd.transform.filter.agregation_spatiale import Agregation_spatiale
from ptd.transform.filter.fenetrage import Fenetrage
from ptd.transform.filter.selection_variable import Selection_variable

from ptd.transform.fusionner.fusion_colone import FusionColonne
from ptd.transform.fusionner.fusion_ligne import FusionLigne

from ptd.transform.maths.centrage import Centrage
from ptd.transform.maths.normalisation import Normalisation
from ptd.transform.maths.moyenneGlissante import MoyenneGlissante

from ptd.export.exportationCsv import ExportationCsv
from ptd.export.exportationJson import ExportationJson

if __name__ == '__main__':
    # # 1 - Premier Test effectué sur le rapport :
    # # Import json.gz + agrégation spatiale + export csv

    # importation1=ImportJsongz("P:/cours/projet_info/code/data","2013-03.json.gz")
    # table1=importation1.importer()
    # print(table1.get_donnees("region")[0:10])
    
    region_corresp = [
        ['Occitanie','national'],
        ['Île-de-France','national'],
        ['Centre-Val de Loire','national'],
        ['Bourgogne-Franche-Comté','national'],
        ['Nouvelle-Aquitaine','national'],
        ["Provence-Alpes-Côte d'Azur","national"],#
        ['Auvergne-Rhône-Alpes','national'],
        ["Bretagne","national"],
        ['Hauts-de-France','national'],
        ["Pays de la Loire","national"],
        ['Normandie','national'],
        ['Grand Est','national']
    ]

    # region_corresp = [
    #     ['Occitanie','Sud France métropolitaine'],
    #     ['Île-de-France','Nord France métropolitaine'],
    #     ['Centre-Val de Loire','Nord France métropolitaine'],
    #     ['Bourgogne-Franche-Comté','Nord France métropolitaine'],
    #     ['Nouvelle-Aquitaine','Sud France métropolitaine'],
    #     ["Provence-Alpes-Côte d'Azur","Sud France métropolitaine"],
    #     ['Auvergne-Rhône-Alpes','Sud France métropolitaine'],
    #     ["Bretagne","Nord France métropolitaine"],
    #     ['Hauts-de-France','Nord France métropolitaine'],
    #     ["Pays de la Loire","Nord France métropolitaine"],
    #     ['Normandie','Nord France métropolitaine'],
    #     ['Grand Est','Nord France métropolitaine']
    # ]
    
    # a1=Agregation_spatiale(region_corresp,"region","date")
    # table2=a1.agreger(table1)
    # print(table2.body[0:10])
    # print(table2.header)
    # export1=ExportationCsv("out_test1", "P:/cours/projet_info/")
    # export1.exporter_table(table2)

    # # 2 - Dexuième test sur le rapport :
    # # Import json.gz + normalisation + affichage + export json

    # importation2=ImportJsongz("P:/cours/projet_info/code/data","2014-06.json.gz")
    # table3=importation2.importer()
    # print(table3.affichage())
    # transfo1 = Normalisation("consommation_brute_electricite_rte")
    # print("avant : ",table3.get_donnees("consommation_brute_electricite_rte")[0:10])
    # table4 = transfo1.normaliser(table3)
    # print("après : ",table4.get_donnees("consommation_brute_electricite_rte")[0:10])
    # print(table4.affichage())
    # export2 = ExportationJson("out_test2","P:/cours/projet_info/")
    # export2.exporter_table(table4)

    # # 3- Troisième test fonctionnel du rapport
    # # Import csv.gz *2 + fusion ligne + moyenne glissante + graphiques
    
    # importation3 = ImportCsvgz("P:/cours/projet_info/code/data/","synop.201707.csv.gz")
    # table5 = importation3.importer
    # print(table5)
    # print(table5.get_donnees("numer_sta")[0:10])
    # print("1e table : ",table5.affichage())
    # importation4 = ImportCsvgz("P:/cours/projet_info/code/data/","synop.201708.csv.gz")
    # table5_bis = importation4.importer()
    # print("2e table : ",table5_bis.affichage())
    # fusion=FusionLigne(table5)
    # table6 = fusion.rbind(table5_bis)
    # print("table fusionnée : ",table6.affichage())
    # transfo2 = MoyenneGlissante("pmer",6)
    # print("avant : ",table6.get_donnees("pmer")[0:10])
    # transfo2.moyenne_mobile(table6)
    # print("après : ",table6.get_donnees("pmer")[0:10])
    # transfo2 = ConversionType("rr1",0)
    # print("avant : ",table6.get_donnees("rr1")[0:10])
    # transfo2.convertir(table6)
    # print("après : ",table6.get_donnees("rr1")[0:10])
    # Graphiques.scatter(table6,"pmer","rr1",title = "My Title",xlab="pmer",ylab="rr1")
    # Graphiques.hist(table6,"pmer",title="Hist :)",xlab="rr1")
    
    # # 4- Quatrième test fonctionnel du rapport
    # # Import csv d'un fichier qu'on avait exporté + graphiques
    
    # importation4=ImportCsv("P:/cours/projet_info/","out_test1.csv",",")#mettez votre lien
    # table7=importation4.importer()
    # print(table7.header)
    # Graphiques.scatter(table7," consommation_brute_electricite_rte"," consommation_brute_gaz_totale",title="beau titre",\
    #     xlab="conso elec",ylab="conso totale")
    # Graphiques.hist(table7," consommation_brute_electricite_rte",title="beau titre",xlab="conso elec")
    # print(table7.affichage())
    # print(table7)

    # # 5- Cinquième test fonctionnel du rapport
    # # Import Json d'un fichier qu'on avait exporté + fenetrage + selection variable + export au format csv
    
    # importation5 = ImportJson("P:/cours/projet_info/","out_test2.json") #mettez votre lien
    # table8 = importation5.importer()
    # print("import json : ",table8.affichage())
    # print(table8.get_donnees("date"))
    # filtrage2 = Fenetrage("date","2014-06-15","2014-06-27")
    # table9 = filtrage2.selection(table8)
    # print(table9.affichage())
    # filtrage3 = Selection_variable(["region","code_insee_region","date","heure","erreur_volontaire",\
    #     "consommation_brute_totale","statut_rte","consommation_brute_electricite_rte"])
    # table10 = filtrage3.selection(table9)
    # print("sélection après fenêtrage : ", table10.affichage())
    # print(table10.body[-1])
    # Graphiques.hist(table10,"consommation_brute_electricite_rte",title="beau titre",xlab="conso elec rte")
    # export3=ExportationCsv("out_test5", "P:/cours/projet_info/")
    # export3.exporter_table(table10)
    
    # # 6- Nouveau test fonctionnel
    # # fusion de colonnes + graphiques
    # table11 = ImportCsvgz("P:/cours/projet_info/code/data/","synop.201509.csv.gz").importer()
    # table12 = ImportJsongz("P:/cours/projet_info/code/data/","2015-09.json.gz").importer()
    # tableau_corres2 = ImportCsv("P:/cours/projet_info/code/data/","postesSynopAvecRegions.csv").importer()
    # print(tableau_corres2.affichage())
    # tableau_corres2=Selection_variable(["ID","Region"]).selection(tableau_corres2)
    # print(tableau_corres2)
    # fus= FusionColonne(table11,"numer_sta",tableau_corres2)
    # table_fusionnee=fus.merge(table12,"region")
    # Graphiques.scatter(table_fusionnee,"consommation_brute_electricite_rte","tminsol",title="beau titre",\
    #      xlab="conso elec rte",ylab="temperature minimale au sol (F)")


    

    