#imports des packages et classes :
import numpy

from ptd.model.table import Table
from ptd.graphiques import Graphiques

from ptd.importation.importJsongz import ImportJsongz
from ptd.importation.importJson import ImportJson
from ptd.importation.importCsvgz import ImportCsvgz
from ptd.importation.importCsv import ImportCsv

from ptd.transform.changements.conversion_type import ConversionType
from ptd.transform.changements.renommer import Renommer
from ptd.transform.changements.mise_a_jour import MiseAJour
from ptd.transform.changements.ajouter_colonne import AjouterColonne
from ptd.transform.changements.ajouter_ligne import AjouterLigne
from ptd.transform.changements.supprimer_colonne import SupprimmerColonne
from ptd.transform.changements.supprimer_ligne import SupprimmerLigne

from ptd.transform.filter.agregation_spatiale import Agregation_spatiale
from ptd.transform.filter.fenetrage import Fenetrage
from ptd.transform.filter.selection_variable import Selection_variable

from ptd.transform.fusionner.fusion_colone import FusionColonne
from ptd.transform.fusionner.fusion_ligne import FusionLigne

from ptd.transform.maths.centrage import Centrage
from ptd.transform.maths.normalisation import Normalisation
from ptd.transform.maths.moyenneGlissante import MoyenneGlissante

from ptd.export.exportationCsv import ExportationCsv
from ptd.export.exportationJson import ExportationJson

if __name__ == '__main__':
    ma_table = ImportCsvgz("P:/cours/projet_info/source/données_météo","synop.202003.csv.gz").importer()
    