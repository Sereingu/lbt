#imports des packages et classes :
import numpy

from ptd.model.table import Table
from ptd.graphiques import Graphiques

from ptd.importation.importJsongz import ImportJsongz
from ptd.importation.importJson import ImportJson
from ptd.importation.importCsvgz import ImportCsvgz
from ptd.importation.importCsv import ImportCsv

from ptd.transform.changements.conversion_type import ConversionType
from ptd.transform.changements.renommer import Renommer
from ptd.transform.changements.mise_a_jour import MiseAJour
from ptd.transform.changements.ajouter_colonne import AjouterColonne
from ptd.transform.changements.ajouter_ligne import AjouterLigne
from ptd.transform.changements.supprimer_colonne import SupprimmerColonne
from ptd.transform.changements.supprimer_ligne import SupprimmerLigne

from ptd.transform.filter.agregation_spatiale import Agregation_spatiale
from ptd.transform.filter.fenetrage import Fenetrage
from ptd.transform.filter.selection_variable import Selection_variable

from ptd.transform.fusionner.fusion_colone import FusionColonne
from ptd.transform.fusionner.fusion_ligne import FusionLigne

from ptd.transform.maths.centrage import Centrage
from ptd.transform.maths.normalisation import Normalisation
from ptd.transform.maths.moyenneGlissante import MoyenneGlissante

from ptd.export.exportationCsv import ExportationCsv
from ptd.export.exportationJson import ExportationJson

if __name__ == '__main__':
    # Question type : Quel effet les premiers mois de 
    # confinements ont eu sur le climat et la consommation? (17 mars au 12 mai 2020)
    # On peut vouloir se concentrer sur uniquement quelques variables du climat,
    # mais toutes de la consommation.

    # 1. Import des données
    conso03=ImportJsongz("P:/cours/projet_info/source/données_électricité","2020-03.json.gz").importer()
    conso04=ImportJsongz("P:/cours/projet_info/source/données_électricité","2020-04.json.gz").importer()
    conso05=ImportJsongz("P:/cours/projet_info/source/données_électricité","2020-05.json.gz").importer()
    climat03=ImportCsvgz("P:/cours/projet_info/source/données_météo","synop.202003.csv.gz").importer()
    climat04=ImportCsvgz("P:/cours/projet_info/source/données_météo","synop.202004.csv.gz").importer()
    climat05=ImportCsvgz("P:/cours/projet_info/source/données_météo","synop.202005.csv.gz").importer()
    # print("consommation : " ,conso03.affichage())
    # print("--------------------------------------")
    # print("climat : ", climat03.affichage())

    # 2. Fenêtrage sur le début du confinement :
    # print(conso03.get_donnees("date")[0:10])
    # print(climat03.get_donnees("date")[0:10])
    conso03 = Fenetrage("date","2020-03-17").selection(conso03)
    climat03 = Fenetrage("date","20200317000000").selection(climat03)#pire format ever
    # print("consommation : " ,conso03.affichage())
    # print("--------------------------------------")
    # print("climat : ", climat03.affichage())

    # On pourrait faire de même pour récupérer les données de mai uniquement jusqu'au 12.
    # Mais les effets du confinement peuvent potentiellement continuer après le confinement lui-même
    # C'est un choix que l'utilisateur fait ;)

    # 3. Fusion par thème : fusion lignes
    conso = FusionLigne(conso03).rbind(FusionLigne(conso04).rbind(conso05))
    climat = FusionLigne(climat03).rbind(FusionLigne(climat04).rbind(climat05))
    # print("consommation : " ,conso.affichage())
    # print("--------------------------------------")
    # print("climat : ", climat.affichage())

    # 4. Sélection des variables voulues sur le climat et sur la consommation :
    climat = Selection_variable(["numer_sta","date","ff","t","u","n","pres","etat_sol"]).selection(climat)
    SupprimmerColonne("code_insee_region").supprimer(conso) #ça m'embêtait :>
    # print("consommation : " ,conso.affichage())
    # print("--------------------------------------")
    # print("climat : ", climat.affichage())

    # 5. On renomme les variables précédentes parce que j'aime pas leur nom
    Renommer(["ff","t","u","n","pres"],\
        ["vitesse_vent_moyenne","temperature","humidite","nebulosite_totale","pression"]).renommer(climat)
    # print("climat : ", climat.affichage())

    # 7 : On voit que des variables sont au mauvais type dans la table Climat 'o'
    ConversionType("vitesse_vent_moyenne").convertir(climat)
    ConversionType("temperature").convertir(climat)
    ConversionType("humidite").convertir(climat)
    ConversionType("nebulosite_totale").convertir(climat)
    ConversionType("pression").convertir(climat)
    # print("Climat : ",climat.affichage())

    # 8 : Gestion de la différence entre pas de récolte des données pour ces données
    # Due à des pas différents, la fusion de ces deux thèmes est compliquée.
    # Nous utilisons Agregation spatiale de manière détournée afin d'avoir une ligne par jour
    ma_date = list(set(conso.get_donnees("date")))
    correspondance = numpy.empty(shape=(len(ma_date),2),dtype=str)
    correspondance[:,0] = ma_date
    correspondance[:,1] = ma_date
    conso = Agregation_spatiale(correspondance,"date","region").agreger(conso)
    
    # print(conso.affichage())

    date = climat.get_donnees("date")
    climat.body[:,climat.get_index_var("date")] = list(item[0:8] for item in date)
    ma_date = list(set(climat.get_donnees("date")))
    correspondance = numpy.empty(shape=(len(ma_date),2),dtype=str)
    correspondance[:,0] = ma_date
    correspondance[:,1] = ma_date
    climat = Agregation_spatiale(correspondance,"date","numer_sta").agreger(climat)

    # print(len(set(climat.get_donnees("numer_sta"))))#Il y a 61 stations
    # print(climat.affichage())

    # 9. On lie les données sur le climat et la consommation

    # 9.1) On récupère le tableau de correspondances entre stations et régions
    correspondance = ImportCsv("P:/cours/projet_info/code/data/","postesSynopAvecRegions.csv").importer()
    correspondance = Selection_variable(["ID","Region"]).selection(correspondance)
    # print(correspondance.affichage())

    # 9.2) On fusionne!
    confinement = FusionColonne(climat,"numer_sta",correspondance).merge(conso,"region")
                            #   table , variable  , tableau/array        table, variable
    # print(confinement.affichage())

    # 10. On enregistre tout ça
    ExportationCsv("donnees_confinement_region","P:/cours/projet_info/").exporter_table(confinement)

    # 10.bis) Backup :D
    # confinement = ImportCsv("P:/cours/projet_info/","donnees_confinement_region.csv",delimiteur=",").importer()
    
    # 11. Continuons le traitement par une agrégation spatiale au niveau national

    # 11.1) Création d'un nouveau tableau de correspondance
    region_corresp = [
        ['Occitanie','national'],
        ['Île-de-France','national'],
        ['Centre-Val de Loire','national'],
        ['Bourgogne-Franche-Comté','national'],
        ['Nouvelle-Aquitaine','national'],
        ["Provence-Alpes-Côte d'Azur","national"],#
        ['Auvergne-Rhône-Alpes','national'],
        ["Bretagne","national"],
        ['Hauts-de-France','national'],
        ["Pays de la Loire","national"],
        ['Normandie','national'],
        ['Grand Est','national']
    ]

    # 11.2) Agréger selon la date (un petit plus)
    confinement_national = Agregation_spatiale(region_corresp,"region","date").agreger(confinement)
    print(confinement_national.affichage())

    # 12. Opérations mathématiques sur quelques variables :

    # 12.1) Normalisation sur la pression (en Pascal)
    confinement_national = Normalisation("pression").normaliser(confinement_national)

    # 12.2) Moyenne Glissante sur une semaine de la consommation électrique totale
    MoyenneGlissante("consommation_brute_electricite_rte",7).moyenne_mobile(confinement_national)

    # 13. Export du résultat :)
    ExportationCsv("donnees_confinement_national","P:/cours/projet_info/").exporter_table(confinement_national)

    # 14. Quelques graphiques !

    # 14.1) Nuage de points
    Graphiques.scatter(confinement_national,"consommation_brute_totale","temperature",\
        title="À partir du confinement",xlab="Consommation totale",ylab="Température (K)")

    # 14.2) Histogramme
    Graphiques.hist(confinement_national,"pression",title="Pression (Centrée réduite)",xlab=" ")


    

    