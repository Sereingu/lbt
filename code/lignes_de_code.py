#Ne pas lancer ce script!

#imports des packages et classes :
import numpy

from ptd.model.table import Table
from ptd.graphiques import Graphiques

from ptd.importation.importJsongz import ImportJsongz
from ptd.importation.importJson import ImportJson
from ptd.importation.importCsvgz import ImportCsvgz
from ptd.importation.importCsv import ImportCsv

from ptd.transform.changements.conversion_type import ConversionType
from ptd.transform.changements.renommer import Renommer
from ptd.transform.changements.mise_a_jour import MiseAJour
from ptd.transform.changements.ajouter_colonne import AjouterColonne
from ptd.transform.changements.ajouter_ligne import AjouterLigne
from ptd.transform.changements.supprimer_colonne import SupprimmerColonne
from ptd.transform.changements.supprimer_ligne import SupprimmerLigne

from ptd.transform.filter.agregation_spatiale import Agregation_spatiale
from ptd.transform.filter.fenetrage import Fenetrage
from ptd.transform.filter.selection_variable import Selection_variable

from ptd.transform.fusionner.fusion_colone import FusionColonne
from ptd.transform.fusionner.fusion_ligne import FusionLigne

from ptd.transform.maths.centrage import Centrage
from ptd.transform.maths.normalisation import Normalisation
from ptd.transform.maths.moyenneGlissante import MoyenneGlissante

from ptd.export.exportationCsv import ExportationCsv
from ptd.export.exportationJson import ExportationJson

#Lignes de code, exemples :

### Création d'une table et fonctionnalités usuelles
super_table = Table(["donatien","lydia","banruo","louis","tessa"],\
    numpy.array([["a",1,1,8,3],["a",2,0,4,3],["b",0,8,9,2],["b",2,8,7,3],["c",1,5,1,2],["c",2,3,1,4]],dtype=object))
print(super_table.affichage())
print(super_table.header)
print(super_table.body)
print(super_table.type_donnees())
print(super_table.summary("lydia"),super_table.summary("donatien"))
print(super_table.get_donnees("banruo"))

### Imports
super_man = ImportCsvgz("P:/Justice_League","superman.csv.gz").importer()
batman = ImportJsongz("P:/Justice_League","2017-batman.json.gz").importer()
wonder_woman1 = ImportCsv("P:/Justice_League","wonderwoman_film1.csv").importer()
wonder_woman1984 = ImportCsv("P:/Justice_League","wonderwoman_film2.csv").importer()

### Fusiooon

wonder_woman = FusionLigne(wonder_woman1).rbind(wonder_woman1984)
#                          table de dessus      table de dessous

Batman_vs_SuperMan = FusionColonne(batman,"num_gadget",\
#                                  table_gauche, sa variable de lien
    comparaison_pouvoir_gadget).merge(super_man,"num_pouvoir")
#   tableau de correspondance  .     table de droite, sa variable de lien

### Filtrer

Batman_vs_SuperMan = Agregation_spatiale(regroupement_des_sequences_par_film,"film_sequence","combat",0).agreger(Batman_vs_SuperMan)
#                                        tableau de correspondance            variable,       variable discriminante

moment_favori_wonder_woman1 = Fenetrage("sequences","date_debut_combat_village","date_lancer_de_tank").selection(wonder_woman1)
#                                       variable date, date debut              , date fin                        table

batman = Selection_variable(["les","fleurs","egal","vie"]).selection(batman)
#                         liste de variables à garder                table

### Opérations méthématiques
super_man = Centrage("duree_de_vol_par_film").centrer(super_man)
#                     ma_variable_a_centrer           table

print("durée moyenne de vol par film : ",Centrage("duree_de_vol_par_film").calcul_moyenne(super_man))
#                                                  ma_variable                            table

batman = Normalisation("duree_combats_par_film").normaliser(batman)
#                       ma_variable_a_normaliser            table

print("Ecart-type de ma_variable : ",Normalisation("duree_combats_par_film").calcul_ecart_type(batman))
#                                                   ma_variable

MoyenneGlissante("duree_suspense",5).moyenne_mobile(Batman_vs_SuperMan)
#                 ma_variable                       table

### Changements

ConversionType("i_am_a_number_i_swear",0).convertir(batman) 
# variable à convertir, son nouveau type .          table

Renommer(["Being","Rickrolled","is","somewhat","fine"],["never","gonna","give","you","up"]).renommer(wonder_woman) 
#          anciennes variables                        , nouvelles variables                          table

MiseAJour(["donatien","lydia"],[["a",1],["b",2]],"louis",[3,4]).modifier(super_table) 
# clefs primaires, valeurs_clefs_primaires, variable à modifier, nouvelles valeurs

AjouterColonne("note_ptd",[20,20,20,20,20]).ajouter(super_table)
#               nouvelle variable     données

AjouterLigne(["b",1,0,5,3,20]).ajouter(super_table)
#            nouvelles valeurs      table

SupprimmerColonne("duree_de_vol_par_film").supprimer(super_man)
#                  nom variable à supprimer          table

SupprimmerLigne(["donatien","lydia"],["a",2]).supprimer(super_table)
#                clefs primaires     valeurs primaires  table