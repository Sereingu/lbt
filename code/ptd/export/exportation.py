'''
Script contenant la classe Exportation
'''

import doctest
from abc import ABC, abstractmethod
from ptd.model.table import Table

class Exportation(ABC):
    '''
    Classe abstraite mère définissant les exports

    Attributs
    --------
    nom_fichier : str
    nom du fichier à sauvegarder sur l'ordinateur
    adresse_dossier : str
    adresse où sauvegarder le fichier
    typeF : str
    type selon lequel exporter le fichier
    '''

    def __init__(self, nom_fichier, adresse_dossier, typeF):
        '''Constructeur'''
        if typeF in ['.json', '.csv']:
            self.type = typeF
        else:
            assert Exception(
                'Les formats disponibles pour sauvegarder le fichier sont : .json, .csv')
        self.adresse_dossier = adresse_dossier
        self.nom_fichier = nom_fichier

    @abstractmethod
    def exporter_table(self, table: Table):
        '''méthode abstraite'''
        pass

    def __str__(self):
        return "Exportation d'une table sous le nom " + self.nom_fichier +\
             ",\n à l'adresse " + self.adresse_dossier + "."


if __name__ == '__main__':
    doctest.testmod()
