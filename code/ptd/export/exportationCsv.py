'''
Script contenant la classe Exportation_csv
'''

import doctest
import numpy
from ptd.export.exportation import Exportation
from ptd.model.table import Table

class ExportationCsv(Exportation):
    '''
    Permet d'exporter un fichier en CSV

    Attributs
    --------
    nom_fichier : str
    nom du fichier à sauvegarder sur l'ordinateur
    adresse_dossier : str
    adresse où sauvegarder le fichier
    delimiteur : str 
    délimiteur du fichier csv que l'utilisateur souhaite utiliser,
    par défaut = ","

    Examples
    --------
    >>> export1=ExportationCsv("essai", "P:/cours/projet_info/")
    >>> t1 = Table(["v1","v2"],numpy.array([[0,1],[2,3],[3,4]],dtype=object))
    >>> export1.exporter_table(t1)
    '''

    def __init__(self, nom_fichier, adresse_dossier, delimiteur = ","):
        ''' Constructeur'''
        super().__init__(nom_fichier, adresse_dossier, ".csv")
        self.delimiteur = delimiteur

    def exporter_table(self, table: Table):
        '''
        Permet d'exporter une table au format csv

        Attributs
        ---------
        table : Table
        la table à enregistrer sur l'ordinateur

        Examples
        --------
        >>> export1=ExportationCsv("essai", "P:/cours/projet_info/")
        >>> t1 = Table(["v1","v2"],numpy.array([[0,1],[2,3],[3,4]],dtype=object))
        >>> export1.exporter_table(t1)
        '''
        # Vérfication rapide du chemin vers les données
        # Si l'utilisateur a mis un '/', on lie l'adresse du dossier au fichier.
        #   Sinon, on en ajoute un avant le lien.
        if self.adresse_dossier[-1] == "/":
            link = self.adresse_dossier + self.nom_fichier + self.type
        else:
            link = self.adresse_dossier + "/" + self.nom_fichier + self.type

        # Le nom des variables est mis au bon format pour l'export : les listes sont refusées.
        header = ', '.join(table.header)

        # Récupération le format des données pour l'export
        type_header = table.type_donnees()

        # Exportation via numpy.savetxt
        # En précisant '.csv', cela enregistre le fichier au format csv directement
        numpy.savetxt(link, table.body, delimiter=self.delimiteur,
                      header=header, comments='', fmt=type_header)


if __name__ == '__main__':
    doctest.testmod()
