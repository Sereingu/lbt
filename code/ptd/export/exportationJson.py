'''
Script contenant la classe Exportation_json
'''

import doctest
import json
import numpy
from ptd.export.exportation import Exportation
from ptd.model.table import Table


class ExportationJson(Exportation):
    '''
    Permet d'exporter un fichier au format JSON

    Attributs
    --------
    nom_fichier : str
    nom du fichier à sauvegarder sur l'ordinateur
    adresse_dossier : str
    adresse où sauvegarder le fichier
    
    Examples
    --------
    >>> export1=ExportationJson("essai2", "P:/cours/projet_info/")
    >>> t1 = Table(["v1","v2"],numpy.array([[0,1],[2,3],[3,4]],dtype=object))
    >>> export1.exporter_table(t1)
    '''

    def __init__(self, nom_fichier, adresse_dossier):
        ''' Constructeur'''
        super().__init__(nom_fichier, adresse_dossier, ".json")

    def exporter_table(self, table: Table):
        '''
        Permet d'exporter la table choisie

        Attributs
        ---------
        table : Table
        le tableau de données à enregistrer sur l'ordinateur
        '''
        # Vérfication rapide du chemin vers les données
        # Si l'utilisateur a mis un '/', on lie l'adresse du dossier au fichier.
        #   Sinon, on en ajoute un avant le lien.
        if self.adresse_dossier[-1] == "/":
            link = self.adresse_dossier + self.nom_fichier + self.type
        else:
            link = self.adresse_dossier + "/" + self.nom_fichier + self.type

        # Transformation des données au format json
        # La fonction utilisée nécessite le format [{},{},...,{}] pour l'enregistrement.
        # Pour une éventuelle réimportation plus tard, si implémentation dans notre code,
        #   nous utilisons un sous-dictionnaire par ligne 'fields' pour enregistrer les données.
        donnees_json = []
        for i in range(len(table.body)):
            field_ligne = {}
            for j in range(len(table.header)):
                nom_variable = table.header[j]
                field_ligne[nom_variable] = table.body[i, j]
            donnees_json.append({
                'indice': i,
                'fields': field_ligne
            })

        # Exportation dans le nouveau fichier json
        # https://stackoverflow.com/questions/27102573/need-help-exporting-data-to-json-file
        with open(link, "w") as outfile:
            json.dump(donnees_json, outfile)


if __name__ == '__main__':
    doctest.testmod()
