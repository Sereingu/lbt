'''
Script contenant la classe Table
'''

import doctest
from matplotlib import pyplot
import scipy
import numpy
from ptd.model.table import Table


class Graphiques:
    '''
    Permet de créer et observer le résultat de graphiques,
    selon une Table et un nom de colonne, au moins.
    '''

    def scatter(table : Table, var1, var2, var3 = None, couleurs = None, title = None, xlab = None, ylab = None):
        '''
        Permet d'afficher un nuage de points
        selon les variables var1 et var2 de la table.
        L'utilisateur a le choix de changer la couleur.

        Attributs
        ---------
        table : Table
        la table contenant les données qui vont être utilisées
        var1 : str
        nom de la première variable quantitative (abscisse) utilisée
        var2 : str
        nom de la deuxième variable quantitative (ordonnée) utilisée
        var3 : str
        nom de la variable controlant la taille des points
        Par défaut = None
        couleurs : list(str) or str
        soit un nom de couleur valide, soit une liste de couleurs 
        de la longueur des données (nombre d'observations)
        Par défaut = None
        title : str
        le titre du graphique
        Par défaut = None
        xlab : str
        nom pour l'axe des abscisses
        Par défaut = None
        ylab : str
        nom pour l'axe des ordonnées
        Par défaut = None
        '''
        if var1 not in table.header or var2 not in table.header :
            print("Au moins une des variables n'existe pas dans la table.\
                Conseil : vérifier l'orthographe.")
            return
        donnees_var1 = table.get_donnees(var1)
        donnees_var2 = table.get_donnees(var2)
        pyplot.title(title)
        if var2 in table.header and var3 in table.header :
            donnees_var3 = table.get_donnees(var3)
            pyplot.scatter(donnees_var1,donnees_var2, s=donnees_var3, color=couleurs)
        else : pyplot.scatter(donnees_var1, donnees_var2, color=couleurs)
        pyplot.xlabel(xlab)
        pyplot.ylabel(ylab)
        pyplot.show()

    def hist(table : Table, var, couleur_var = None,\
         title = None, xlab = None, ylab = "Count"):
        '''
        Permet d'afficher un nuage de points
        selon les variables var1 et var2 de la table.
        L'utilisateur a le choix de changer la couleur.

        Attributs
        ---------
        table : Table
        Table contenant les données à utiliser
        var : str
        nom de la variable quantitative utilisée
        couleur_var : str
        un nom de couleur valide pour la variable
        Par défaut = None
        title : str
        le titre du graphique
        Par défaut = None
        xlab : str
        nom pour l'axe des abscisses
        Par défaut = None
        ylab : str
        nom pour l'axe des ordonnées
        Par défaut = "Count"
        '''
        if var not in table.header : 
            print("La variable n'existe pas dans la table.\
                Conseil : vérifier l'orthographe.")
            return
        
        donnees_var = numpy.array(table.get_donnees(var),dtype=float)

        pyplot.title(title)
        try :
            pyplot.hist(donnees_var, color=couleur_var, alpha = 0.9)
        except :
            donnees_var = donnees_var[~numpy.isnan(donnees_var)]
            pyplot.hist(donnees_var, color=couleur_var, alpha = 0.9)
        pyplot.xlabel(xlab)
        pyplot.ylabel(ylab)
        pyplot.grid()
        pyplot.show()

if __name__ == '__main__':
    doctest.testmod()