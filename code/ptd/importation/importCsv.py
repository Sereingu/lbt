import csv
import doctest
import numpy
from ptd.importation.importation import Importation
from ptd.model.table import Table


class ImportCsv(Importation):
    """
    Permet d'importer une table au format Csv
    Classe fille de Importation

    Parameters
    ----------
    adresse_dossier : str
        le répertoire du fichier
    nom_fichier : str
        le nom du fichier dans l'ordinateur
    delimiteur : str
        le delimiteur du fichier : 1-character string, valeur par defaut ';'
    """

    def __init__(self, adresse_dossier, nom_fichier, delimiteur=";"):
        """constructeur"""
        super().__init__(adresse_dossier, nom_fichier, ".csv")
        self.delimiteur = delimiteur

    def importer(self):
        """
        Importer un fichier de format .csv

        Returns
        -------
        resultat : Table
        le fichier importé au format table
        """
        # generer le repertoire
        if self.adresse_dossier[-1] == "/":
            link = self.adresse_dossier + self.nom_fichier
        else:
            link = self.adresse_dossier + "/" + self.nom_fichier

        resultat = []
        with open(link, mode='rt') as file:
            synopreader = csv.reader(file, delimiter=self.delimiteur)
            for row in synopreader:
                resultat.append(row)

        # on a donc resultat qui contient le jeux de donnees en format liste de liste
        
        # Récupération de la liste nom_varialbe
        nom_variable = resultat[0]
        longeur = len(resultat)
        table_valeur = resultat[1:longeur]

        # Gestion des valeurs manquantes
        exemples_manquantes = ['mq',' ','NA','','NaN','Nan',\
            'NAN','nan','None','**','***','*']
        for i in range(len(table_valeur)):
            for j in range(len(nom_variable)):
                if table_valeur[i][j] in exemples_manquantes:
                    table_valeur[i][j] = numpy.nan

        # Retour du résultat sous format Table
        return Table(nom_variable,\
             numpy.array(table_valeur, dtype=object))

if __name__ == '__main__':
    doctest.testmod()