import doctest
import gzip
import json
import numpy
from ptd.importation.importation import Importation
from ptd.model.table import Table

class ImportJsongz(Importation):
    """
    Permet d'importer une table au format Json
    Classe fille de Importation

    Parameters
    ----------
    adresse_dossier : str
        le répertoire du fichier
    nom_fichier : str
        le nom du fichier dans l'ordinateur
    """

    def __init__(self, adresse_dossier, nom_fichier):
        """constructeur"""
        super().__init__(adresse_dossier, nom_fichier, ".json.gz")

    def importer(self):
        """
        Importer un fichier de format .json.gz

        Returns
        -------
        resultat: Table
        """
        if self.adresse_dossier[-1] == "/":
            link = self.adresse_dossier + self.nom_fichier
        else:
            link = self.adresse_dossier + "/" + self.nom_fichier

        with gzip.open(link, mode='rt', encoding="UTF-8") as gzfile:
            resultat = json.load(gzfile)
        # "resultat" contient le jeux de donnees en format dictionnaire
        # On va donc convertir ce dictionnaire en liste.

        # Premièrement, on va collecter les noms de variables,
        # car python néglige les clefs (nom variable) avec données manquantes
        tmp_a = list(resultat[0]["fields"])
        for i in range(1, len(resultat)):
            tmp_b = list(resultat[i]["fields"])
            tmp_a = list(set(tmp_a + tmp_b))
        liste_nom_variable = tmp_a

        # On range les valeurs
        # 1er for : on passe chaque ligne de dictionnaire
        # 2e for : on passe chaque case d'une ligne en comparant la liste_nom_variable
        #    et on insère un indice de donnée manquante (numpy.nan) si on ne trouve pas la variable
        objet_na = numpy.nan
        table_valeur = [[objet_na]*len(liste_nom_variable)]*len(resultat)
        for j in range(len(resultat)):
            lnom_var = list(resultat[j]["fields"])
            lvaleur = list(resultat[j]["fields"].values())
            table_valeur[j] = [objet_na]*len(liste_nom_variable)
            for i in range(len(liste_nom_variable)):
                # On va voir si un élément dans la liste_nom_variable appartient a la liste lnom_var
                if liste_nom_variable[i] in lnom_var:
                    indice = lnom_var.index(liste_nom_variable[i])
                    valeur_correspondant = lvaleur[indice]
                    table_valeur[j][i] = valeur_correspondant

        # Retour du résultat
        return Table(liste_nom_variable, numpy.array(table_valeur, dtype=object))

if __name__ == '__main__':
    doctest.testmod()