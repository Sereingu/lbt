'''
Script contenant la classe Importation
'''

from abc import ABC, abstractmethod


class Importation(ABC):
    '''
    Construire un objet de la classe Importation

    Parameters
    ----------
    adresse_dossier : str
        le répertoire du fichier, sans nom du fichier
    nom_fichier : str
        le nom du fichier dans l'ordinateur
    type : str
        le format du fichier
    '''

    def __init__(self, adresse_dossier, nom_fichier, typeF):
        """constructeur"""
        if typeF in ['.json.gz', '.csv.gz', '.csv', '.json']:
            self.type = typeF
        else:
            assert Exception(
                'Les formats disponibles du fichier à traiter sont :\
                     .json.gz, .csv.gz, .csv, .json')
        self.adresse_dossier = adresse_dossier
        self.nom_fichier = nom_fichier

    @abstractmethod
    def importer(self):
        pass
