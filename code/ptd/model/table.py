'''
Script contenant la classe Table
'''

import doctest
import numpy

class Table:
    '''
    Permet de définir un tableau de données,
    avec ses noms de variables et ses données

    Attributs
    --------
    header : list(str)
    nom des variables
    body : list(list())
    données contenues dans le tableau

    Examples
    --------
    >>> t1=Table(["var1","var2"],numpy.array([[0,1],[2,6],[3,4],[4,8],[5,5]]))
    >>> print(t1.get_donnees("var1"))
    [0 2 3 4 5]
    >>> print(t1.affichage())
    Votre table contient 2 variables et 5 observations.
    Description des variables :
    var1 - Type : float - Nombre de données manquantes (NA) : 0
    var2 - Type : float - NA : 0
    >>> print(t1)
    Variables en lignes (max 10 individus imprimés) :
    [0 2 3 4 5]
    [1 6 4 8 5]
    '''

    def __init__(self, header, body):
        ''' Constructeur'''
        self.header = header
        self.body = body

    def affichage(self):
        '''
        Permet d'afficher un résumé des données

        Examples
        --------
        >>> t1=Table(["var1","var2"],numpy.array([[0,1],[2,6],[3,4],[4,8],[5,5]]))
        >>> print(t1.affichage())
        Votre table contient 2 variables et 5 observations.
        Description des variables :
        var1 - Type : float - Nombre de données manquantes (NA) : 0
        var2 - Type : float - NA : 0
        '''
        # récupération du type des données pour les afficher
        type_donnees = self.type_donnees()
        type_donnees = numpy.char.replace(type_donnees, "%s", "string")
        type_donnees=  numpy.char.replace(type_donnees, "%.5f", "float")

        # données globales sur les données
        nb_observation = len(self.body[:, 0])
        nb_variable = len(self.header)
        desc_head = "Votre table contient " + \
            str(nb_variable) + " variables et " + \
            str(nb_observation) + " observations.\n"

        # informations sur chaque variable
        # permet de récupérer le nombre de données manquantes malgré le conflit avec dtype=object
        nb_na = 0
        for j in range(len(self.body[:, 0])):
            if (self.body[j, 0] != self.body[j, 0]) and isinstance(self.body[j, 0], float):
                nb_na += 1
        desc_var = self.header[0] + " - Type : " + type_donnees[0] +\
            " - Nombre de données manquantes (NA) : " + str(nb_na)
        if nb_variable > 1:
            for i in range(1, nb_variable):
                nb_na = 0
                for j in range(len(self.body[:, i])):
                    if (self.body[j, i] != self.body[j, i]) and isinstance(self.body[j, i], float):
                        nb_na += 1
                desc_var += "\n" + self.header[i] + " - Type : " +\
                     type_donnees[i] + " - NA : " + str(nb_na)

        return desc_head + "Description des variables :\n" + desc_var

    def get_index_var(self, nom_variable):
        '''
        Permet de récupérer l'index de la variable recherchée

        Attributs
        --------
        nom_variable : str
        nom de la variable dont on veut récupérer l'index

        Returns
        --------
        resultat : int
        index de la colonne recherchée
        '''
        if nom_variable in self.header:
            num_colonne = (self.header).index(nom_variable)
            return num_colonne
        else:
            print("La variable " + nom_variable +
                  " n'existe pas dans la table sélectionnée.")

    def get_donnees(self, nom_variable):
        '''
        Permet de récupérer des données contenue dans une variable

        Attributs
        --------
        nom_variable : str
        nom de la variable par rapport à laquelle on veut récupérer les données

        Returns
        --------
        resultat : list()
        données de la variable correspondante

        Examples
        --------
        >>> t1=Table(["var1","var2"],numpy.array([[0,1],[2,6],[3,4],[4,8],[5,5]]))
        >>> print(t1.get_donnees("var1"))
        [0 2 3 4 5]
        '''
        num_colonne = self.get_index_var(nom_variable)
        return self.body[:, num_colonne]

    def type_donnees(self):
        '''
        Permet de récupérer le format des données

        Returns
        --------
        type_header : list()
        liste contenant le format simplifié des données

        Examples
        --------
        >>> t1=Table(["var1","var2"],numpy.array([[0,1],[2,6],[3,4],[4,8],[5,5]],dtype=object))
        >>> print(t1.type_donnees())
        ['%.5f', '%.5f']
        '''

        if "" in self.header :
            self.header.remove("")

        # Les données manquantes "NaN" ne sont pas prises en compte pour déterminer le format :
        #   Si une est trouvée, on passe à la ligne suivante jusqu'à trouver une observation
        #       pour la colonne.
        #   Pour éviter d'arriver sur la dernière ligne et d'avoir toujours une donnée
        #       manquante, nous revenons à la première ligne après chaque variable (j=0).
        #   Pour pallier le conflit entre numpy.isnan() et le type object, nous revenons
        #       à la défintion des données manquantes : un float qui n'est pas égal à lui-même.
        # Les données étant des nombres entiers ou décimaux sont enregistrés en tant que
        #   décimaux (float) afin d'éviter les conflits avec les données manquantes. Les
        #   autres sont conservées en tant que chaînes de caractères (string).
        type_header = []
        j = 0
        for i in range(len(self.header)):
            while (self.body[j, i] != self.body[j, i]) and isinstance(self.body[j, i], float):
                j += 1
                if j == len(self.body[:,0]) :
                    type_header.append("%s")
                    i += 1
                    j = 0
            if isinstance(self.body[j, i], (float,int)):
                type_header.append("%.5f")
            else:
                type_header.append("%s")
            j = 0
        return type_header
    
    def summary(self, nom_variable):
        '''
        Permet d'obtenir un résumé simple du contenu des données, 
        il ne sert pas à démontrer la distribution d'une variable 
        quantitative, mais à vérifier les types de valeur d'une variable.
        Par exemple, il est possible de vérifier si des données de 
        type caractères et des données numériques appartiennent à la même variable.

        Attributs
        --------
        nom_variable : str
        nom de la variable par rapport à laquelle on veut récupérer les données

        Returns
        --------
        resultat : Dictionary
        Dans le dictionnaire, les clés correspond à la valeur de la variable, 
        et la valeur de clé est le nombre de fois où la valeur de la variable 
        apparaît. Pour toutes les valeurs numériques, elles sont regroupées 
        dans une seule représentation clé.

        '''
        # vérification nom de la variable 
        if nom_variable not in self.header:
            print("Le nom de variable dont valeurs à resumer n'existe pas dans la Table.")
            return
        
        index_var = self.get_index_var(nom_variable)

        # résumé les valeurs
        nb_na=0
        resultat={"NA" : nb_na, "numeric" : 0}
        
        for j in range(len(self.body[:, 0])):
            # si une valeur est manquante, on l'ajoute à son compteur
            if (self.body[j, index_var] != self.body[j, index_var])\
                 and isinstance(self.body[j, index_var], float):
                nb_na += 1
            # si une valeur est de type str, on la compte aussi
            elif isinstance(self.body[j, index_var], str):
                if self.body[j, index_var] not in resultat.keys():
                    resultat[self.body[j, index_var]] = 1
                else:
                    resultat[self.body[j, index_var]] += 1
            # si c'est une variable numérique, on incrémente le compteur
            else:
                resultat["numeric"] +=1
        
        # on attribue le nombre de données manquantes au dictionnaire
        resultat["NA"]=nb_na

        # on retourne le dictionnaire
        return resultat

    def __str__(self):
        desc = "Variables en lignes (max 10 individus imprimés) :\n" + \
            str(self.body[:, 0])
        nb_variable = len(self.header)
        if len(self.body[:, 0]) >= 10:
            nb_individus_imprimer = 10
        else:
            nb_individus_imprimer = len(self.body[:, 0])
        if nb_variable > 1:
            for i in range(1, nb_variable):
                desc += "\n" + str(self.body[0:nb_individus_imprimer, i])
        return desc

if __name__ == '__main__':
    doctest.testmod()
