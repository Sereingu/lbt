'''
Script contenant la classe Ajouter_colonne
'''

import doctest
import numpy
from ptd.model.table import Table
from ptd.transform.changements.changements import Changements

class AjouterColonne(Changements):
    '''
    Classe permettant d'ajouter une colonne dans la table

    Attributs
    ---------
    nom_variable : str
    le nom de la variable à ajouter
    valeurs : list()
    les valeurs à ajouter au format str ou float (ou int)
    '''

    def __init__(self, nom_variable, valeurs):
        '''Constructeur'''
        self.nom_variable = nom_variable
        self.valeurs = valeurs

    def ajouter(self, table: Table):
        '''
        Ajouter une colonne dans la table

        Ajout automatique à la table existante
        (i.e. cette fonction ne renvoie rien)

        Attributs
        ---------
        table : Table
        la table à laquelle une nouvelle colonne doit être ajoutée.

        Examples
        --------
        >>> t1 = Table(["v1","v2"],numpy.array([["a",1],["b",0],["c",4]],dtype=object))
        >>> AjouterColonne("v3",[0,3,6]).ajouter(t1)
        >>> print(t1)
        Variables en lignes (max 10 individus imprimés) :
        ['a' 'b' 'c']
        [1 0 4]
        [0 3 6]
        '''

        # vérification nom de la variable
        if self.nom_variable in table.header:
            print("Le nom de la nouvelle variable que vous avez entré apparaît déjà, " +
                  "veuillez vous assurer que vous n'ajoutez pas une variable en double " +
                  "et donnez-lui un nouveau nom.")
            return

        # vérification de la taille de liste
        if len(table.body[:, 0]) != len(self.valeurs):
            print("Veuillez saisir à nouveau les valeurs à ajouter, en veillant à ce " +
                  "qu'elle soit de la même taille que la table .")
            return

        # trasnformation des données en numpy.array
        if isinstance(self.valeurs, list):
            self.valeurs = numpy.array(self.valeurs)
            try:
                self.valeurs.shape = (len(self.valeurs), len(self.valeurs[0]))
            except:
                self.valeurs.shape = (len(self.valeurs), 1)

        # ajout des valeurs et du nom de colonne
        table.header.append(self.nom_variable)
        table.body = numpy.hstack((table.body, self.valeurs))


if __name__ == '__main__':
    doctest.testmod()
