'''
Script contenant la classe Ajouter_ligne
'''

import doctest
import numpy
from ptd.model.table import Table
from ptd.transform.changements.changements import Changements


class AjouterLigne(Changements):
    '''
    Classe permettant d'ajouter une ligne de la table

    Attributs
    ---------
    valeurs : list
    les valeurs à ajouter
    '''

    def __init__(self, valeurs):
        '''Constructeur'''
        self.valeurs = valeurs

    def ajouter(self, table: Table):
        '''
        Ajoute une ligne dans la table spécifiée

        Attributs
        ---------
        table : Table
        la table à laquelle une nouvelle ligne doit être ajoutée.

        Examples
        --------
        >>> t1=Table(["v1","v2"],numpy.array([["a",1],["a",2]],dtype=object))
        >>> AjouterLigne(["b",1]).ajouter(t1)
        >>> print(t1)
        Variables en lignes (max 10 individus imprimés) :
        ['a' 'a' 'b']
        [1 2 '1']
        '''

        # vérification de la taille de liste
        if len(table.header) != len(self.valeurs):
            print("Veuillez saisir à nouveau la valeur à ajouter," +
                  " en veillant à ce qu'elle soit de la même taille " +
                  "que le nombre de variables dans la table.")
            return

        # transformation des valeurs en numpy.array
        if isinstance(self.valeurs, list):
            self.valeurs = numpy.array(self.valeurs)
            try:
                self.valeurs.shape = (len(self.valeurs[0]), len(self.valeurs))
            except:
                self.valeurs.shape = (1, len(self.valeurs))

        # ajout des valeurs
        table.body = numpy.vstack((table.body, self.valeurs))


if __name__ == '__main__':
    doctest.testmod()
