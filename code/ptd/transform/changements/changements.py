'''
Script contenant la classe mère Changements
'''

from ptd.transform.transformation import Transformation


class Changements(Transformation):
    '''
    Classe mère permettant d'effectuer des modifications simples sur une Table choisie
    '''
