'''
Script contenant la classe Renommer
'''

import doctest
import numpy
from ptd.model.table import Table
from ptd.transform.changements.changements import Changements


class ConversionType(Changements):
    '''
    Classe permettant de renommer au moins une variable

    Modifie le header d'une Table choisie

    Attributs
    ---------
    nom_variable : str
    le nom de la variables dont les données sont à changer de type
    nouveau_type : int
    le nouveau type de la variable
    0 pour float, 1 pour str
    par défaut 0 (float)
    '''

    def __init__(self, nom_variable, nouveau_type=0):
        '''Constructeur'''
        self.nom_variable = nom_variable
        self.nouveau_type = nouveau_type

    def convertir(self, table: Table):
        '''
        Modifie le type des données d'une variable contenue dans une table

        Attributs
        ---------
        table : Table
        la table contenant la variable dont les données sont à modifier

        Examples
        --------
        >>> t1=Table(["var1","var2"],numpy.array([[0,1],[2,6],[3,7]],dtype=object))
        >>> r1 = ConversionType("var1",1)
        >>> r1.convertir(t1)
        >>> print(t1.get_donnees("var1"))
        ['0' '2' '3']
        '''
        # vérification nom de la variable
        if self.nom_variable not in table.header:
            print("Le nom de variable indiqué n'existe pas dans la Table.")
            return

        index_var = table.get_index_var(self.nom_variable)
        if self.nouveau_type == 0:
            try:
                table.body[:, index_var] = numpy.array(
                    table.get_donnees(self.nom_variable), dtype=float)
            except:
                print("La conversion au type float de " +
                      self.nom_variable + " n'a pas fonctionné.")
        else:
            try:
                table.body[:, index_var] = numpy.array(
                    table.get_donnees(self.nom_variable), dtype=str)
            except:
                print("La conversion au type string de " +
                      self.nom_variable + " n'a pas fonctionné.")


if __name__ == '__main__':
    doctest.testmod()
