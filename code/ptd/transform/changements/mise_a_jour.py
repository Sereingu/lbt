'''
Script contenant la classe Mise_a_jour
'''

import doctest
import numpy
from ptd.model.table import Table
from ptd.transform.changements.changements import Changements


class MiseAJour(Changements):
    '''
    Classe permettant de modifier la valeur d'une variable

    Il est possible de modifier la valeur pour une seule ligne
    ou de modifier pour plusieurs lignes (chaque valeur de chaque
    ligne pouvant être différente).

    Attributs
    ---------
    nom_variable_identite : list(str)
    le nom de la/les variables de clefs primaires (permettant d'identifier la/les lignes)
    identite : list(list())
    les valeurs permettant d'identifier la/les lignes à modifier
        exemple - si une seule ligne est à modifier : ['a',1]
        exemple - si deux lignes sont à modifier : [['a',1],['b',5]]
    nom_variable : str
    le nom de la variable dont les données sont à modifier pour la/les lignes identifiées
    valeur : list()
    la nouvelle valeur (sous liste) de la/les ligne(s) correspondante(s)
        ces valeurs peuvent être différentes, mais doivent être rangées dans l'ordre de
        l'identification des lignes
    '''

    def __init__(self, nom_variable_identite, identite, nom_variable, valeur):
        '''Constructeur'''
        self.nom_variable_identite = nom_variable_identite
        self.identite = identite
        self.nom_variable = nom_variable
        self.valeur = valeur

    def modifier(self, table: Table):
        '''
        Modifier une valeur d'une ou plusieurs ligne(s) selon identité indiquée

        Attributs
        ---------
        table : Table
        la table contenant la variable dont les données sont à modifier

        Examples
        --------
        >>> t1=Table(["v1","v2","v3","v4"],numpy.array([["a",0,5,6],\
            ["a",1,7,6],["b",0,4,7],["b",1,4,2],\
            ["c",0,4,8],["b",2,5,9],["c",1,5,7]],dtype=object))
        >>> t2=t1
        >>> MiseAJour(["v1","v2"],["a",1],"v3",[0]).modifier(t2)
        Vous avez modifié 1 ligne au total.
        >>> print(t2.body)
        [['a' 0 5 6]
         ['a' 1 0 6]
         ['b' 0 4 7]
         ['b' 1 4 2]
         ['c' 0 4 8]
         ['b' 2 5 9]
         ['c' 1 5 7]]
        >>> t2=t1
        >>> MiseAJour(["v1","v2"],[["a",1],["c",0]],"v3",[0,0]).modifier(t2)
        Vous avez modifié 2 ligne(s) au total.
        >>> print(t2.body)
        [['a' 0 5 6]
         ['a' 1 0 6]
         ['b' 0 4 7]
         ['b' 1 4 2]
         ['c' 0 0 8]
         ['b' 2 5 9]
         ['c' 1 5 7]]
        >>> t2=t1
        >>> MiseAJour(["v1","v2"],[["a",1],["c",0]],"v3",[0,1]).modifier(t2)
        Vous avez modifié 2 ligne(s) au total.
        >>> print(t2.body)
        [['a' 0 5 6]
         ['a' 1 0 6]
         ['b' 0 4 7]
         ['b' 1 4 2]
         ['c' 0 1 8]
         ['b' 2 5 9]
         ['c' 1 5 7]]
        '''

        # vérification nom de la variable et les noms de la variable d'identité
        # et récupération les indices correspondantes
        if self.nom_variable not in table.header:
            print("Le nom de variable dont valeur à changer n'existe pas dans la Table.")
            return
        index_var = table.get_index_var(self.nom_variable)

        index_var_i = []
        for nom in self.nom_variable_identite:
            if nom not in table.header:
                print(nom + " n'existe pas dans la Table.")
                return
            index_var_i.append(table.get_index_var(nom))

        # vérification d'existence de l'identité et remplacer la valeur
        self.identite = numpy.array(self.identite, dtype=object)
        if self.identite.shape == (len(self.identite),):
            index_modification = numpy.all(
                table.body[:, index_var_i] == self.identite, axis=1)
            index_modification = numpy.where(index_modification)[0]
            print("Vous avez modifié 1 ligne au total.")
            table.body[index_modification, index_var] = self.valeur
        else:
            nb_modification = 0
            for i, ligne_a_modifier in enumerate(self.identite):
                index_modification = numpy.all(
                    table.body[:, index_var_i] == ligne_a_modifier, axis=1)
                index_modification = numpy.where(index_modification)[0]
                if len(index_modification) >= 1:
                    table.body[index_modification, index_var] = self.valeur[i]
                    nb_modification += 1
                else:
                    print("L'identité " + str(ligne_a_modifier) +
                          " n'existe pas dans la table.")
            print("Vous avez modifié " +
                  str(nb_modification) + " ligne(s) au total.")


if __name__ == '__main__':
    doctest.testmod()
