'''
Script contenant la classe Renommer
'''

import doctest
import numpy
from ptd.model.table import Table
from ptd.transform.changements.changements import Changements


class Renommer(Changements):
    '''
    Classe permettant de renommer au moins une variable

    Modifie le header d'une Table choisie

    Attributs
    ---------
    anciennes_variables : list(str)
    la liste des noms des variables dont il faut modifier le nom
    nouvelles_variables : list(str)
    la liste des nouveaux noms de variables
    '''

    def __init__(self, anciennes_variables, nouvelles_variables):
        '''Constructeur'''
        self.anciennes_variables = anciennes_variables
        self.nouvelles_variables = nouvelles_variables

    def renommer(self, table: Table):
        '''
        Change le header d'une table selon les nouveaux noms donnés.

        Attributs
        ---------
        table : Table
        la table contenant les variables à modifier

        Examples
        --------
        >>> t1=Table(["var1","var2"],numpy.array([[0,1],[2,6],[3,7]],dtype=object))
        >>> r1 = Renommer(["var1"],["ma_var1"])
        >>> r1.renommer(t1)
        >>> print(t1.header)
        ['ma_var1', 'var2']
        '''
        # vérification list
        longueur_old = len(self.anciennes_variables)
        longueur_new = len(self.nouvelles_variables)
        if longueur_new != longueur_old:
            print("Les deux listes ne sont pas de la même longueur.")
            return

        # vérification d'existance
        for i in range(longueur_old):
            if self.anciennes_variables[i] in table.header:
                j = table.get_index_var(self.anciennes_variables[i])
                table.header[j] = self.nouvelles_variables[i]
            else:
                print(
                    "Le nom " + self.anciennes_variables[i] + " n'existe pas dans la table.")


if __name__ == '__main__':
    doctest.testmod()
