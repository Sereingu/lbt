'''
Script contenant la classe Supprimmer_colonne
'''

import doctest
import numpy
from ptd.model.table import Table
from ptd.transform.changements.changements import Changements


class SupprimmerColonne(Changements):
    '''
    Classe permettant de supprimer une colonne de la table

    Attributs
    ---------
    nom_variable : str
    le nom de la variables à supprimer

    '''

    def __init__(self, nom_variable):
        '''Constructeur'''
        self.nom_variable = nom_variable

    def supprimer(self, table: Table):
        '''
        Supprimer une colonne dans la table spécifiée

        Attributs
        ---------
        table : Table
        la table à laquelle une nouvelle colonne doit être supprimée.

        Examples
        --------
        >>> t1 = Table(["v1","v2","v3","v4"],numpy.array([[1,2,3,4],[2,3,4,5]]))
        >>> SupprimmerColonne("v3").supprimer(t1)
        >>> print(t1)
        Variables en lignes (max 10 individus imprimés) :
        [1 2]
        [2 3]
        [4 5]
        '''

        # vérification nom de la variable
        if self.nom_variable not in table.header:
            print(
                "Le nom de variable dont valeur à supprimer n'existe pas dans la Table.")
            return
        index_var = table.get_index_var(self.nom_variable)

        # suppression la colonne
        table.header.pop(index_var)  # dans l'header
        table.body = numpy.delete(table.body, index_var, 1)  # dans les données


if __name__ == '__main__':
    doctest.testmod()
