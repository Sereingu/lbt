'''
Script contenant la classe Supprimmer_ligne
'''

import doctest
import numpy
from ptd.model.table import Table
from ptd.transform.changements.changements import Changements

class SupprimmerLigne(Changements):
    '''
    Classe permettant de supprimer une ligne de la table

    Attributs
    ---------
    nom_variable_identite : list(str)
    le nom de la/les variable(s) primaires (permettant d'identifier une ligne)
    identite : list
    valeurs permettant d'identifer la ligne à indexer pour la suppression

    '''

    def __init__(self, nom_variable_identite, identite):
        '''Constructeur'''
        if len(nom_variable_identite) != len(identite):
            print("Veuillez recréer l'objet en vous assurant que l'identité et les variables \
            correspondant à l'identité ont la même longueur")
            return
        self.nom_variable_identite = nom_variable_identite
        self.identite = identite

    def supprimer(self, table: Table):
        '''
        Supprimemr une ou plusieurs ligne(s) dans la table

        Attributs
        ---------
        table : Table
        la table à laquelle une nouvelle ligne doit être supprimée.

        Examples
        --------
        >>> t1=Table(["v1","v2","v3","v4"],numpy.array([["a",0,5,6],\
            ["a",1,7,6],["b",0,4,7],["b",1,4,2],\
            ["c",0,4,8],["b",2,5,9],["c",1,5,7]],dtype=object))
        >>> SupprimmerLigne(["v1","v2"],[["b",1],["c",1]]).supprimer(t1)
        Vous avez supprimé 2 ligne(s) au total
        >>> print(t1.body)
        [['a' 0 5 6]
         ['a' 1 7 6]
         ['b' 0 4 7]
         ['c' 0 4 8]
         ['b' 2 5 9]]
        '''

        # vérification les noms de la variable d'identité et récupération
        #   les indices correspondantes
        index_var_i = []
        for nom in self.nom_variable_identite:
            if nom not in table.header:
                print(nom + \
                    " de variable d'indentité indiqué n'existe pas dans la Table.")
                return
            index_var_i.append(table.get_index_var(nom))

        # vérification d'existence de l'identité et remplacer la valeur
        self.identite = numpy.array(self.identite, dtype=object)
        if self.identite.shape == (len(self.identite),):
            index_suppression = numpy.all(
                table.body[:, index_var_i] == self.identite, axis=1)
            index_suppression = numpy.where(index_suppression)[0]
            print("Vous avez supprimé 1 ligne au total.")
            table.body = numpy.delete(table.body, index_suppression, 0)
        else:
            nb_suppression = 0
            for ligne_a_supprimer in self.identite:
                index_suppression = numpy.all(
                    table.body[:, index_var_i] == ligne_a_supprimer, axis=1)
                index_suppression = numpy.where(index_suppression)[0]
                if len(index_suppression) >= 1:
                    table.body = numpy.delete(
                        table.body, index_suppression[0], 0)
                    nb_suppression += 1
                else:
                    print("L'identité " + str(ligne_a_supprimer) +
                          " n'existe pas dans la table.")
            print("Vous avez supprimé " +
                  str(nb_suppression) + " ligne(s) au total")


if __name__ == '__main__':
    doctest.testmod()
