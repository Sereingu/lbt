'''
Script contenant la classe Agregation_spatiale
'''

import doctest
import numpy
from ptd.transform.filter.filtrage import Filtrage
from ptd.model.table import Table
from ptd.transform.filter.selection_variable import Selection_variable

class Agregation_spatiale(Filtrage):
    '''
    Classe permettant d'agréger les données d'une table selon un lien géographique

    Premet d'agréger les données d'une table grâce au lien géographique entre deux
    listes, représenté par un tableau, peut découper plus finement l'agrégation
    selon une autre variable, et agrège selon le choix de l'utilisateur
    (moyenne : 0, médiane : 1, somme : 2).
    Classe fille de Filtrage

    Attributs
    --------
    tableau_correspondance : list(list(str))
    tableau représentant le lien géographique entre la variable de la table
        (1e colonne), et la nouvelle variable (2e colonne)
    nom_variable_agregat : str
    variable sur laquelle on doit effectuer l'agrégation spatiale
    variable_discriminante : str (="" par défaut)
    variable qui découpe plus précisément les données (par exemple: la date)
    choix_agregation : int (= 0 par défaut)
    choix de l'utilisateur selon la manière d'agréger :
        0 (moyenne), 1 (médiane), 2 (somme)
    '''

    def __init__(self, tableau_correspondance, nom_variable_agregat,\
        variable_discriminante="", choix_agregation=0):
        ''' Constructeur'''
        self.tableau_correspondance = tableau_correspondance
        self.nom_variable_agregat = nom_variable_agregat
        self.variable_discriminante = variable_discriminante
        self.choix_agregation = choix_agregation

    def agreger(self, table: Table):
        '''
        Permet d'agréger des données d'une table

        Attributs
        ---------
        table : Table
        La table contenant les données devant être modifées

        Return
        --------
        new_table: Table
        nouvelle table agrégée

        Examples
        --------
        >>> t1=Table(header=["var1","var2","var3","var4"],body=numpy.array([\
            ['a',0,4,6],\
            ['b',2,8,19],\
            ['c',0,7,6],\
            ['d',1,20,3],\
            ['a',1,14,36],\
            ['b',2,5,2],\
            ['c',0,0,40],\
            ['d',1,61,49],\
            ['a',0,2,1],\
            ['b',1,8,29],\
            ['c',3,41,16],\
            ['d',2,0,1],\
            ['e',0,2,1],\
            ['e',1,8,29],\
            ['e',3,41,16],\
            ['f',2,0,1]\
        ],dtype=object))
        >>> tableau = [\
            ['a','M'],\
            ['b','N'],\
            ['c','O'],\
            ['d','N'],\
            ['e','O'],\
            ['f','N']\
        ]
        >>> a1=Agregation_spatiale(tableau_correspondance=tableau,nom_variable_agregat="var1",\
            variable_discriminante="var2")
        >>> print(a1.agreger(t1).body)
        Toutes les variables demandées sont déjà dans le tableau, sans variables supplémentaires.
        [['M' '0' 3.0 3.5]
         ['M' '1' 14 36]
         ['N' '1' 29.666666666666668 27.0]
         ['N' '2' 3.25 5.75]
         ['O' '0' 3.0 15.666666666666666]
         ['O' '1' 8 29]
         ['O' '3' 41.0 16.0]]
        '''
        # Vérification de l'appartenance de la colonne d'agrégation à la table
        if self.nom_variable_agregat not in table.header:
            print(self.nom_variable_agregat + " n'existe pas dans la table.")
            return

        # Tableau de correspondance au format numpy array
        # Pour l'utilisation de certaines fonctions par la suite, cette opération est nécessaire
        self.tableau_correspondance = numpy.array(self.tableau_correspondance)
        nb_new_agregats = self.tableau_correspondance.shape[0]

        # Remplacer les anciens agrégats par les nouveaux
        # https://www.geeksforgeeks.org/how-to-replace-values-in-a-list-in-python/
        old_agregats = table.get_donnees(self.nom_variable_agregat).tolist()
        new_agregats = numpy.char.replace(old_agregats, self.tableau_correspondance[0][0],\
                                   self.tableau_correspondance[0][1])
        for i in range(1, nb_new_agregats):
            new_agregats = numpy.char.replace(new_agregats, self.tableau_correspondance[i][0],
                                       self.tableau_correspondance[i][1])

        # Récupération de l'index de la variable d'agrégation pour alléger l'écriture
        # ET enlever les variables n'étant pas des nombres
        # (exceptées la variable d'agrégation et potentiellement la discriminante)
        # Avec création de la table de renvoi
        type_header = table.type_donnees()
        nom_var_nombre = []
        for i, j in enumerate(type_header):
            if j == "%.5f":
                nom_var_nombre.append(table.header[i])
        if self.variable_discriminante in table.header:
            variables_a_garder = [
                self.nom_variable_agregat, self.variable_discriminante]
            variables_a_garder.extend(nom_var_nombre)
            new_table = Selection_variable(
                list(set(variables_a_garder))).selection(table)
            index_var_discriminante = new_table.get_index_var(
                self.variable_discriminante)
        else:
            variables_a_garder = [self.nom_variable_agregat]
            variables_a_garder.extend(nom_var_nombre)
            new_table = Selection_variable(
                list(set(variables_a_garder))).selection(table)
        index_var_agrega = new_table.get_index_var(self.nom_variable_agregat)

        # Affectation des nouveaux agrégats
        new_table.body[:, index_var_agrega] = new_agregats

        # Affectation de la fonction choisie par l'utilisateur
        if self.choix_agregation == 1:
            fonction = numpy.nanmedian
            fonction2 = numpy.median
        elif self.choix_agregation == 2:
            fonction = numpy.nansum
            fonction2 = numpy.sum
        else:
            fonction = numpy.nanmean
            fonction2 = numpy.mean

        # essai avec total découpage, idée de base et changée provenant de :
        # https://stackoverflow.com/questions/50950231/group-by-with-numpy-mean

        # check pb sur les différences de type :
        # https://stackoverflow.com/questions/30167538/convert-a-numpy-ndarray-to-stringor-bytes-and-convert-it-back-to-numpy-ndarrayv

        # Agrégation des données
        # Difficultés rencontrées avec le type object des données : nous avons procédé à un
        #   découpage total (une double boucle) selon les valeurs uniques de la variable
        #   d'agrégation (et la discriminante).
        # La fonction est appliquée sur les groupes d'observations contenant un seul unique
        #   (ou couple d'uniques avec la discriminante). Puis les résultats sont regroupés.
        # Il est possible lors du découpage qu'un groupe soit composé d'une seule observation.
        #   Dans ce cas, l'observation est directement regroupée (la fonction n'est pas appliquée).
        # Deux cas sont différenciés : si la variable discriminante a été précisée et existe, ou
        #   si elle n'existe pas/n'a pas été précisée par l'utilisateur.
        new_body = []
        if self.variable_discriminante in new_table.header:
            header = [self.nom_variable_agregat, self.variable_discriminante]
            header.extend(numpy.delete(new_table.header,\
                (index_var_discriminante, index_var_agrega)))
            new_table.header = header
            unique_agrega = numpy.unique(new_table.body[:, (index_var_agrega)])
            unique_discriminant = numpy.unique(\
                new_table.body[:, (index_var_discriminante)])
            for i in unique_agrega:
                tmp1 = new_table.body[numpy.where(\
                    new_table.body[:, index_var_agrega] == i)]
                for j in unique_discriminant:
                    tmp2 = tmp1[numpy.where(\
                        tmp1[:, index_var_discriminante] == j)]
                    tmp3 = numpy.delete(\
                        tmp2, (index_var_agrega, index_var_discriminante), axis=1)
                    if len(tmp2) == 1:
                        tmp3 = tmp3[0]
                        new_body.append(numpy.append([i, j], tmp3))
                    elif len(tmp2) > 1:
                        try : tmp3 = fonction(tmp3, axis=0)
                        except : tmp3 = fonction2(tmp3,axis=0)
                        new_body.append(numpy.append([i, j], tmp3))
        else:
            header = [self.nom_variable_agregat]
            header.extend(numpy.delete(new_table.header, index_var_agrega))
            new_table.header = header
            unique_agrega = numpy.unique(new_table.body[:, (index_var_agrega)])
            for j in unique_agrega:
                tmp2 = new_table.body[numpy.where(\
                    new_table.body[:, index_var_agrega] == j)]
                if len(tmp2) == 1:
                    new_body.append(numpy.append([j], numpy.delete(\
                        tmp2, index_var_agrega, axis=1)[0]))
                elif len(tmp2) > 1:
                    try : tmp2 = fonction(numpy.delete(\
                        tmp2, index_var_agrega, axis=1), axis=0)
                    except : tmp2 = fonction2(numpy.delete(\
                        tmp2, index_var_agrega, axis=1), axis=0)
                    new_body.append(numpy.append([j], tmp2))

        # Retourner cette nouvelle table
        new_table.body = numpy.array(new_body)
        return new_table

    def __str__(self):
        return "Je vais agréger une table selon les variables " + self.nom_variable_agregat +\
            " " + str(self.variable_discriminante) + "."

if __name__ == '__main__':
    doctest.testmod()