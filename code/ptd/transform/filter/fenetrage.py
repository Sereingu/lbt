'''
Script contenant la classe Fenetrage
'''

import doctest
import numpy
from ptd.transform.filter.filtrage import Filtrage
from ptd.model.table import Table


class Fenetrage(Filtrage):
    '''
    Permet de sélectionner des données selon une plage de valeurs

    Attributs
    --------
    nom_variable : str
    le nom de la variable contenant les données sur lesquelles la plage est choisie
    date_debut : object = 0
    le début de la plage, par défaut à 0
    date_fin : object = 0
    la fin de la plage, par défaut à 0
    '''

    def __init__(self, nom_variable_date, date_debut=0, date_fin=0):
        ''' Constructeur'''
        self.nom_variable_date = nom_variable_date
        self.date_debut = date_debut
        self.date_fin = date_fin

    def selection(self, table):
        '''
        Permet de sélectionner les données sur une plage donnée

        Attributs
        ---------
        table : Table
        la table sur laquelle on doit effectuer le fenêtrage,
            ses dates sont triées dans le bon ordre

        Return
        --------
        new_table: table
        le résultat de la table fenêtrée

        Examples
        --------
        >>> t1=Table(["var1","var2"],\
            numpy.array([[0,1],[2,6],[3,7],[4,8],[5,10]],dtype=object))
        >>> t2=Table(["var1","var2","var3"],\
            numpy.array([[0,1,2],[1,2,3],[3,4,5],[4,5,12],[5,6,20]],dtype=object))
        >>> s1=Fenetrage("var1",1,4)
        >>> print(s1.selection(t1))
        La valeur de date de début minimale est retournée.
        Variables en lignes (max 10 individus imprimés) :
        [0 2 3 4]
        [1 6 7 8]
        >>> print(s1.selection(t2))
        Variables en lignes (max 10 individus imprimés) :
        [1 3 4]
        [2 4 5]
        [3 5 12]
        '''
        # si les deux dates sont par défaut, ne rien faire car inutile
        if self.date_debut == 0 and self.date_fin == 0:
            print("Vous n'avez pas sélectionné de fenêtrage.")
            return table

        #Vérification de l'appartenance de la colonne dans la table
        if self.nom_variable_date not in table.header:
            print("La colonne indiquée ne se trouve pas dans la table.")

        #création d'une nouvelle table, triée selon la colonne de date
        new_table = Table(table.header,\
            table.body[table.body[:,\
                table.get_index_var(self.nom_variable_date)].argsort()])

        # Récupération des données de date pour une manipulation plus aisée
        donnees = numpy.array(new_table.get_donnees(self.nom_variable_date),\
            dtype=str)
        self.date_debut = str(self.date_debut)
        self.date_fin = str(self.date_fin)

        # Trouver l'indice des occurences si elles existent
        # Notes :
        #   1) numpy.where renvoie une liste, on extrait le premier élément à
        #   l'aide de [0], puis le chiffre à l'aide de int().
        #   2) pour diminuer les coûts de recherche pour la date de fin,
        #   on retourne/inverse la liste (via [::-1]).
        
        # on essaie de trouver la première occurence de la date_min
        list = numpy.where(donnees == self.date_debut)[0]
        if len(list)>=1 :
            first_index_date_debut = list[0]
        else :
            first_index_date_debut = 0
            print("La valeur de date de début minimale est retournée.")

        # on essaie de trouver la dernière occurence de la date_fin
        list = numpy.where(donnees[::-1] == self.date_fin)[0]
        if len(list)>=1 :
            last_index_date_fin = len(table.body[:, 0]) - list[0]
        else :
            last_index_date_fin = len(table.body[:, 0])
            print("La valeur de date de fin maximale est retournée.")

        # On récupère uniquement les observations qui nous intéressent
        new_table.body = new_table.body[first_index_date_debut:last_index_date_fin, :]
        return new_table

    def __str__(self):
        return "Je vais découper une table selon le fenêtrage suivant :" +\
             str(self.date_debut) + "-" + str(self.date_fin)


if __name__ == '__main__':
    doctest.testmod()
