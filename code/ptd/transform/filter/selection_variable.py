'''
Script contenant la classe Selection_variable
'''

import doctest
import numpy
from ptd.transform.filter.filtrage import Filtrage
from ptd.model.table import Table


class Selection_variable(Filtrage):
    '''
    Permet de sélectionner des variables dans une table

    Attributs
    --------
    nom_variable : list(str)
    contient les noms de variables à sélectionner

    Examples
    --------
    >>> t1=Table(["var1","var2","var3"],numpy.array([[0,1,1],[2,6,8],[3,4,7],[4,8,12],[5,5,10]]))
    >>> s1=Selection_variable(["var1"])
    >>> print(s1.selection(t1))
    Variables en lignes (max 10 individus imprimés) :
    [0 2 3 4 5]
    >>> s3=Selection_variable(["var1","var3"])
    >>> print(s3.selection(t1))
    Variables en lignes (max 10 individus imprimés) :
    [0 2 3 4 5]
    [1 8 7 12 10]
    >>> print(s1)
    Je récupère les données des variables suivantes : ['var1'] pour une table donnée.
    '''

    def __init__(self, nom_variable):
        ''' Constructeur'''
        self.nom_variable = nom_variable

    def selection(self, table):
        '''
        Crée une nouvelle table dans laquelle ont été sélectionnées les variables

        Attributs
        ---------
        table : Table
        la table dont on récupère les variables

        Return
        --------
        new_table: Table
        La table triée.

        Examples
        --------
        >>> t1=Table(["var1","var2","var3"],\
            numpy.array([[0,1,1],[2,6,8],[3,4,7],[4,8,12],[5,5,10]]))
        >>> s1=Selection_variable(["var1"])
        >>> print(s1.selection(t1))
        Variables en lignes (max 10 individus imprimés) :
        [0 2 3 4 5]
        >>> s2=Selection_variable(["var1","var4"])
        >>> print(s2.selection(t1))
        var4 n'existe pas dans le tableau de données.
        Variables en lignes (max 10 individus imprimés) :
        [0 2 3 4 5]
        [None None None None None]
        >>> s3=Selection_variable(["var1","var3"])
        >>> print(s3.selection(t1))
        Variables en lignes (max 10 individus imprimés) :
        [0 2 3 4 5]
        [1 8 7 12 10]
        >>> s4=Selection_variable(["var1","var3","var2"])
        >>> t2=s4.selection(t1)
        Toutes les variables demandées sont déjà dans le tableau, sans variables supplémentaires.
        '''
        # Vérifier qu'on prend pas tout, sinon on retourne la table en entier :
        if sorted(self.nom_variable) == sorted(table.header):
            print("Toutes les variables demandées sont déjà dans le tableau,"+\
                " sans variables supplémentaires.")
            return table

        # Création d'une nouvelle table
        new_table = Table(self.nom_variable,\
            numpy.empty((len(table.body[:, 0]), len(self.nom_variable)), dtype=object))

        # vérification que les variables existent dans la table et
        #   récupération de leurs données, tour à tour
        for une_variable in self.nom_variable:
            if une_variable in table.header:
                new_table.body[:, self.nom_variable.index(
                    une_variable)] = table.get_donnees(une_variable)
            else:
                print(str(une_variable) + " n'existe pas dans le tableau de données.")
        return new_table

    def __str__(self):
        return "Je récupère les données des variables suivantes : "\
            + str(self.nom_variable) + " pour une table donnée."


if __name__ == '__main__':
    doctest.testmod()
