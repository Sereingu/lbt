'''
Script contenant la classe Fusion
'''

import doctest
from ptd.transform.transformation import Transformation


class Fusion(Transformation):
    '''
    Classe mère permettant de définir la fusion de tableaux de données
    Classe fille de Transformation, et classe mère de Fusion_ligne et Fusion_colonne

    Attributs
    --------
    table : Table
    Tableau mère sur lequel on souhaite ajouter des données d'un autre tableau
    '''

    def __init__(self, table):
        ''' Constructeur'''
        self.table = table

if __name__ == '__main__':
    doctest.testmod(verbose=True)
