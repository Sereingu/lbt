'''
Script contenant la classe Fusion_ligne
'''

import doctest
import numpy
from ptd.transform.fusionner.fusion import Fusion
from ptd.model.table import Table

class FusionColonne(Fusion):
    '''
    Classe permettant d'ajouter des colonnes à un tableau de données mère

    Classe fille de la classe Fusion

    Attributs
    --------
    table : Table
    Tableau de données mère sur laquelle il faut ajouter les colonnes de données
    fusion_var1 : list(str)
    le nom de la variable contenue dans la table mère permettant de faire la fusion
    tableau_correspondance : list(list(str))
    tableau représentant le lien entre les vairables de fusion de chaque table
        (1e colonne = valeur table mère, 2e colonne = valeur correspondante table2)
    '''

    def __init__(self, table, fusion_var1, tableau_correspondance):
        ''' Constructeur'''
        super().__init__(table)
        if fusion_var1 not in table.header:
            print("Attention : la variable n'existe pas dans la table.")
        self.fusion_var1 = fusion_var1
        if isinstance(tableau_correspondance, Table):
            self.tableau_correspondance = tableau_correspondance.body
        else:
            self.tableau_correspondance = tableau_correspondance

    def merge(self, table2: Table, fusion_var2):
        '''
        Permet de fusionner (ajouter des colonnes d')une deuxième table à la table
        contenue dans Self

        Similaire à merge() dans R.
        Mais c'est un natural join : il ne garde pas les parties non liées

        Attributs
        ---------
        table2 : Table
        la table dont il faut ajouter les colonnes
        fusion_var2 : str
        le nom de la variable contenue dans table2 permettant de faire la fusion
            avec la table mère

        Return
        --------
        new_table: Table
        le résultat des tables fusionnées

        Examples
        --------
        >>> tableau = [\
            ['a','e'],\
            ['b','f'],\
            ['c','g'],\
            ['d',' ']\
            ]
        >>> t1=Table(header=["var1","var2","var3","var4"],\
            body=numpy.array([\
                ['a',0,4,6],\
                ['b',2,8,19],\
                ['c',0,7,6],\
                ['d',1,20,3],\
                ['a',1,14,36],\
                ['b',2,5,2],\
                ['c',0,0,40],\
                ['d',1,61,49],\
                ['a',0,2,1],\
                ['b',1,8,29],\
                ['c',3,41,16],\
                ['d',2,0,1]\
                ],dtype=object))
        >>> t2=Table(header=["var1","var2","var3","var4"],\
            body=numpy.array([\
            ['e',0,2,1],\
            ['e',1,8,29],\
            ['e',2,41,16],\
            ['f',0,0,1],\
            ['f',1,41,16],\
            ['g',0,41,16],\
            ['g',1,41,16]\
            ],dtype=object))
        >>> fus=FusionColonne(t1,'var1',tableau)
        >>> print(fus.merge(t2,'var1').body)
        [['a' 0 4 6 'e' 0 2 1]
         ['a' 1 14 36 'e' 1 8 29]
         ['a' 0 2 1 'e' 2 41 16]
         ['b' 2 8 19 'f' 0 0 1]
         ['b' 2 5 2 'f' 1 41 16]
         ['c' 0 7 6 'g' 0 41 16]
         ['c' 0 0 40 'g' 1 41 16]]
        '''
        # check existance de la variable dans la 2e table
        if fusion_var2 not in table2.header:
            print("La variable de fusion n'existe pas dans la table2")
            return

        # récupération de l'index des variables
        index_var_fusion1 = self.table.get_index_var(self.fusion_var1)
        index_var_fusion2 = table2.get_index_var(fusion_var2)

        # création d'un nouveau corps de données
        new_body = []

        # récupération des données par correspondance
        for row in self.tableau_correspondance:
            index_table1 = numpy.where(
                self.table.body[:, index_var_fusion1] == row[0])[0]
            index_table2 = numpy.where(
                table2.body[:, index_var_fusion2] == row[1])[0]
            for j in range(min(len(index_table1), len(index_table2))):
                ligne = self.table.body[index_table1[j]].tolist()
                ligne.extend(table2.body[index_table2[j]])
                new_body.append(ligne)
        
        header = self.table.header
        header.extend(table2.header)
        new_table = Table(header, numpy.array(new_body, dtype=object))
        return new_table

    def __str__(self):
        return "Ma fonction est d'ajouter des colonnes à la table mère\
             dont les variables sont les suivantes :\n" + str(self.table.header)


if __name__ == '__main__':
    doctest.testmod()
