'''
Script contenant la classe Fusion_ligne
'''

import doctest
import numpy
from ptd.transform.fusionner.fusion import Fusion
from ptd.model.table import Table

class FusionLigne(Fusion):
    '''
    Classe permettant d'ajouter des lignes à un tableau de données mère

    Classe fille de la classe Fusion

    Attributs
    --------
    table : Table
    Tableau de données mère sur laquelle ajouter les lignes de données

    Examples
    --------
    >>> t1=Table(["var1","var2","var3"],numpy.array([[1,2,3],[0,2,0],[11,12,13],[0,1,0]]))
    >>> t2=Table(["var1","var5","var3"],numpy.array([[1,2,3],[0,4,6],[11,12,13]]))
    >>> t3=Table(["var1","var2","var3"],numpy.array([[4,5,6],[4,0,6],[14,15,16]]))
    >>> essai = FusionLigne(t1)
    >>> essai.rbind(t2)#fonctionne! car arrête tout ^^'
    The tables must have the same headers.
    >>> t4=essai.rbind(t3)
    >>> print(t4.body)
    [[ 1  2  3]
     [ 0  2  0]
     [11 12 13]
     [ 0  1  0]
     [ 4  5  6]
     [ 4  0  6]
     [14 15 16]]
    >>> print(essai)
    Ma fonction est d'ajouter des lignes à la table dont les variables sont les suivantes :
    ['var1', 'var2', 'var3']
    '''

    def __init__(self, table):
        ''' Constructeur'''
        super().__init__(table)

    def rbind(self, table2):
        '''
        Permet d'ajouter des lignes d'une deuxième table à la table contenue dans Self

        Similaire à rbind() dans R.

        Attributs
        ---------
        table2 : Table
        la table dont il faut ajouter les lignes

        Return
        --------
        new_table: Table
        le résultat des tables fusionnées

        Examples
        --------
        >>> t1=Table(["var1","var2","var3"],numpy.array([[1,2,3],[0,2,0],[11,12,13],[0,1,0]]))
        >>> t2=Table(["var1","var5","var3"],numpy.array([[1,2,3],[0,4,6],[11,12,13]]))
        >>> t3=Table(["var1","var2","var3"],numpy.array([[4,5,6],[4,0,6],[14,15,16]]))
        >>> essai = FusionLigne(t1)
        >>> essai.rbind(t2)
        The tables must have the same headers.
        >>> t4=essai.rbind(t3)
        >>> print(t4.body)
        [[ 1  2  3]
         [ 0  2  0]
         [11 12 13]
         [ 0  1  0]
         [ 4  5  6]
         [ 4  0  6]
         [14 15 16]]
        '''
        # Vérification que les tableaux ont les mêmes entêtes
        if numpy.any(self.table.header != table2.header):
            print("The tables must have the same headers.")
            return 

        # Vérification si les tables sont rangées dans le même ordre
        # Si ce n'est pas le cas : on réarrange les colonnes
        if self.table.header != table2.header:
            new_body = numpy.empty(
                (len(table2.body[:, 0]), len(table2.header)), dtype=object)
            for i in range(len(self.table.header)):
                new_body[:, i] = table2.get_donnees(self.table.header[i])
            table2.body = new_body

        # On fusionne :
        new_table = Table(self.table.header, numpy.vstack(
            (self.table.body, table2.body)))
        return new_table

    def __str__(self):
        return "Ma fonction est d'ajouter des lignes à la table"+\
            " dont les variables sont les suivantes :\n" + str(self.table.header)

if __name__ == '__main__':
    doctest.testmod()
