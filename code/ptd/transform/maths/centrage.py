'''
Script contenant la classe Table
'''

import doctest
import numpy
from ptd.transform.maths.operation_mathematique import Operation_mathematique
from ptd.model.table import Table


class Centrage(Operation_mathematique):
    '''
    Permet de centrer les données d'une variable donnée contenue dans une table
    Ignore les valeurs manquantes

    Attributs
    --------
    nom_variable : str
    la variable sélectionnée dont les données sont à centrer

    Examples
    --------
    >>> t1=Table(["var1","var2"],\
        numpy.array([[0,1],[2,6],[3,7],[4,8],[5,10]],dtype=float))
    >>> t2=Table(["var1","var2","var3"],\
        numpy.array([[0,1,2],[1,2,3],[3,4,5],[4,5,12],[5,6,20]],dtype=float))
    >>> c1=Centrage("var1")
    >>> c2=Centrage("var3")
    >>> print(c1.centrer(t1))
    Variables en lignes (max 10 individus imprimés) :
    [-2.8 -0.8  0.2  1.2  2.2]
    [ 1.  6.  7.  8. 10.]
    >>> print(c2.centrer(t1))
    La variable var3 n'existe pas dans la table sélectionnée.
    Variables en lignes (max 10 individus imprimés) :
    [-2.8 -0.8  0.2  1.2  2.2]
    [ 1.  6.  7.  8. 10.]
    >>> print(c2.calcul_moyenne(t2))
    8.4
    >>> print(c2.centrer(t2))
    Variables en lignes (max 10 individus imprimés) :
    [0. 1. 3. 4. 5.]
    [1. 2. 4. 5. 6.]
    [-6.4 -5.4 -3.4  3.6 11.6]
    >>> print(round(c2.calcul_moyenne(t2)))
    -0.0
    >>> print(c1)
    Je vais centrer les données de la variable : var1
    '''

    def __init__(self, nom_variable):
        ''' Constructeur'''
        super().__init__(nom_variable)

    def calcul_moyenne(self, table):
        '''
        Calcul la moyenne d'une variable de la table sélectionnée
        Ignore les valeurs manquantes

        Attributs
        ---------
        table : Table
        la table contenant la variable à centrer

        Return
        --------
        Si la variable existe dans la table
        moyenne: float
        la moyenne des données de la variable contenue dans la table

        Examples
        --------
        >>> t2=Table(["var1","var2","var3"],\
            numpy.array([[0,1,2],[1,2,3],[3,4,5],[4,5,12],[5,6,20]],dtype=float))
        >>> c2=Centrage("var3")
        >>> print(c2.calcul_moyenne(t2))
        8.4
        '''
        # nanmean permet d'ignorer les valeurs manquantes
        # mais s'il n'y en a pas, elle ne fonctionne pas!
        if self.nom_variable in table.header:
            try : moyenne = numpy.nanmean(table.get_donnees(self.nom_variable))
            except : moyenne = numpy.mean(table.get_donnees(self.nom_variable))
            return moyenne

    def centrer(self, table: Table):
        '''
        Permet de centrer les données d'une variable de la table sélectionnée

        Attributs
        ---------
        table : Table
        la table contenant les données de la variable à centrer

        Examples
        --------
        >>> t1=Table(["var1","var2"],\
            numpy.array([[0,1],[2,6],[3,7],[4,8],[5,10]],dtype=float))
        >>> t2=Table(["var1","var2","var3"],\
            numpy.array([[0,1,2],[1,2,3],[3,4,5],[4,5,12],[5,6,20]],dtype=float))
        >>> c1=Centrage("var1")
        >>> c2=Centrage("var3")
        >>> print(c1.centrer(t1))
        Variables en lignes (max 10 individus imprimés) :
        [-2.8 -0.8  0.2  1.2  2.2]
        [ 1.  6.  7.  8. 10.]
        >>> print(c2.centrer(t1))
        La variable var3 n'existe pas dans la table sélectionnée.
        Variables en lignes (max 10 individus imprimés) :
        [-2.8 -0.8  0.2  1.2  2.2]
        [ 1.  6.  7.  8. 10.]
        >>> print(c2.centrer(t2))
        Variables en lignes (max 10 individus imprimés) :
        [0. 1. 3. 4. 5.]
        [1. 2. 4. 5. 6.]
        [-6.4 -5.4 -3.4  3.6 11.6]
        '''
        # Permet d'encadrer la fonction :
        # si la variable n'est pas contenue dans la table, on la renvoie telle qu'elle est.
        try:
            moyenne = self.calcul_moyenne(table)
            index_variable = table.get_index_var(self.nom_variable)
            for i in range(0, len(table.body[:, index_variable])):
                table.body[i, index_variable] -= moyenne
            return table
        except:
            return table

    def __str__(self):
        return "Je vais centrer les données de la variable : " + self.nom_variable


if __name__ == '__main__':
    doctest.testmod()
