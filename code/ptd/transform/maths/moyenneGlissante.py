'''
Script contenant la classe Moyenne_glissante
'''

import doctest
import numpy
from ptd.transform.maths.operation_mathematique import Operation_mathematique
from ptd.model.table import Table


class MoyenneGlissante(Operation_mathematique):
    '''
    Permet d'effectuer une moyenne glissante sur des données
    en spécifiant l'ordre et la variable sur laquelle il faut l'appliquer.

    Attributs
    --------
    nom_variable : str
    la variable indiquant les données sur lesquelles il faut appliquer la moyenne glissante
    nombre_glissant : int
    pair ou impair, désigne l'ordre de la moyenne glissante

    Examples
    --------
    >>> t1=Table(["var1","var2"],\
        numpy.array([[0,1],[2,6],[3,7],[4,6],[5,10],[6,11],[7,9],[14,13],[20,0]],dtype=float))
    >>> mm1=Moyenne_glissante("var1",3)
    >>> mm1.moyenne_mobile(t1)
    >>> print(t1.body[:,0])
    [ 0.       1.66667  3.       4.       5.       6.       9.      13.66667
      0.     ]
    >>> mm2=Moyenne_glissante("var2",4)
    >>> mm2.moyenne_mobile(t1)
    >>> print(t1.body[:,1])
    [ 0.    0.    5.    7.25  8.5   9.   10.75  8.25  0.  ]
    >>> print(mm1)
    Je vais effectuer une moyenne glissante d'ordre 3 sur les données de var1
    '''

    def __init__(self, nom_variable, nombre_glissant):
        ''' Constructeur'''
        super().__init__(nom_variable)
        self.nombre_glissant = nombre_glissant

    def moyenne_mobile(self, table: Table):
        '''
        Effectue une moyenne mobile sur une variable d'une table

        Effectue une moyenne mobile d'ordre self.nombre_glissant (int) sur une
        variable (self.nom_variable) dont les données sont contenues dans table
        et change directement les données dans la table.

        Attributs
        ---------
        table : Table
        la table contenant la variable dont les données sont à modifier

        Examples
        --------
        >>> t1=Table(["var1","var2"],\
            numpy.array([[0,1],[2,6],[3,7],[4,6],[5,10],[6,11],[7,9],[14,13],[20,0]],dtype=float))
        >>> mm1=Moyenne_glissante("var1",3)
        >>> mm1.moyenne_mobile(t1)
        >>> print(t1.body[:,0])
        [ 0.       1.66667  3.       4.       5.       6.       9.      13.66667
          0.     ]
        >>> mm2=Moyenne_glissante("var2",4)
        >>> mm2.moyenne_mobile(t1)
        >>> print(t1.body[:,1])
        [ 0.    0.    5.    7.25  8.5   9.   10.75  8.25  0.  ]
        '''
        # Vérifier que la variable est dans la table
        if self.nom_variable not in table.header:
            print("La variable " + self.nom_variable +
                  " n'existe pas dans la table sélectionnée.")
            return
        if (not isinstance(self.nombre_glissant, int)) or self.nombre_glissant == 1:
            print(
                "Le nombre glissant est incorrect : ce n'est pas un entier ou il est trop petit.")
            return

        # Récupération des données et création des variables nécessaires
        nombre_observations = len(table.body[:, 0])
        new_donnees = numpy.zeros((nombre_observations), dtype=float)
        donnees = numpy.array(table.get_donnees(self.nom_variable),dtype=float)

        # Calcul des moyennes glissantes
        # 2 cas :
        #   le nombre glissant (k) est impair :
        #       on recalcule les valeurs de (k-1)/2 à n-1-(k-1)/2 comprises dans la liste
        #       ces valeurs sont au centre du calcul : somme([i-(k-1)/2],...,[i],...[i+(k+1)/2])/k
        #       exemple k=3 : somme([i-1],[i],[i+1])/3
        #           (rappel : sous python l'indice i+2 n'est pas compté)
        #   le nombre glissant (k) est pair :
        #       on recalcule les valeurs de k/2 à n-(k/2) comprises dans la liste
        #       ces valeurs sont à l'indice supérieur du 'milieu' du calcul :
        #           somme([i-(k/2)],...,[i],...[i+(k/2)])/k
        #       exemple k=4 : somme([i-2],[i-1],[i],[i+1])/4
        #           (rappel : sous python l'indice i+2 n'est pas compté)
        if self.nombre_glissant % 2 == 0:
            for i in range(int(self.nombre_glissant/2),
                           int((nombre_observations+1-self.nombre_glissant/2)), 1):
                try : new_donnees[i] = numpy.nanmean(\
                    donnees[int(i-self.nombre_glissant/2):int(i+self.nombre_glissant/2)])
                except : new_donnees[i] = numpy.mean(\
                    donnees[int(i-self.nombre_glissant/2):int(i+self.nombre_glissant/2)])
        elif self.nombre_glissant % 2 == 1:
            for i in range(int((self.nombre_glissant-1)/2),
                           int((nombre_observations-(self.nombre_glissant-1)/2)), 1):
                try : new_donnees[i] = numpy.nanmean(\
                    donnees[i-int((self.nombre_glissant-1)/2):i+int((self.nombre_glissant+1)/2)])
                except : new_donnees[i] = numpy.mean(\
                    donnees[i-int((self.nombre_glissant-1)/2):i+int((self.nombre_glissant+1)/2)])

        # Remplacement des valeurs dans la table
        table.body[:, table.get_index_var(self.nom_variable)] = numpy.round_(
            new_donnees, decimals=5)

    def __str__(self):
        return "Je vais effectuer une moyenne glissante d'ordre " + str(self.nombre_glissant) +\
            " sur les données de " + self.nom_variable


if __name__ == '__main__':
    doctest.testmod()
