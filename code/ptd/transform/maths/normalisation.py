'''
Script contenant la classe Normalisation
'''

import doctest
import numpy
from ptd.transform.maths.operation_mathematique import Operation_mathematique
from ptd.transform.maths.centrage import Centrage
from ptd.model.table import Table


class Normalisation(Operation_mathematique):
    '''
    Permet de calculer l'écart-type et les données normalisées d'une variable

    Attributs
    --------
    nom_variable : str
    la variable dont les données sont à normaliser

    Examples
    --------
    >>> t1=Table(["var1","var2"],numpy.array([[0,1],[2,6],[3,7],[4,8],[5,10]],dtype=float))
    >>> t2=Table(["var1","var2","var3"],\
        numpy.array([[0,1,2],[1,2,3],[3,4,5],[4,5,12],[5,6,20]],dtype=float))
    >>> n1=Normalisation("var1")
    >>> n2=Normalisation("var3")
    >>> print(n1.calcul_ecart_type(t1))
    1.7204650534085253
    >>> print(n2.calcul_ecart_type(t1))
    None
    >>> print(n1.normaliser(t1).body)
    [[-1.62746694  1.        ]
     [-0.46499055  6.        ]
     [ 0.11624764  7.        ]
     [ 0.69748583  8.        ]
     [ 1.27872403 10.        ]]
    >>> print(n2.normaliser(t1).body)
    La variable var3 n'existe pas dans la table sélectionnée.
    [[-1.62746694  1.        ]
     [-0.46499055  6.        ]
     [ 0.11624764  7.        ]
     [ 0.69748583  8.        ]
     [ 1.27872403 10.        ]]
    >>> print(n2.normaliser(t2).body)
    [[ 0.          1.         -0.94527391]
     [ 1.          2.         -0.79757486]
     [ 3.          4.         -0.50217676]
     [ 4.          5.          0.53171657]
     [ 5.          6.          1.71330895]]
    '''

    def __init__(self, nom_variable):
        ''' Constructeur'''
        super().__init__(nom_variable)

    def calcul_ecart_type(self, table):
        '''
        Permet de calculer l'écart-type d'une variable
        Ignore les données manquantes

        Attributs
        ---------
        table : Table
        le tableau contenant les données dont l'écart-type est à calculer

        Return
        --------
        ecart_type : float
        l'écart-type des données

        Examples
        --------
        >>> t1=Table(["var1","var2"],\
            numpy.array([[0,1],[2,6],[3,7],[4,8],[5,10]],dtype=float))
        >>> t2=Table(["var1","var2","var3"],\
            numpy.array([[0,1,2],[1,2,3],[3,4,5],[4,5,12],[5,6,20]],dtype=float))
        >>> n1=Normalisation("var1")
        >>> n2=Normalisation("var3")
        >>> print(n1.calcul_ecart_type(t1))
        1.7204650534085253
        >>> print(n2.calcul_ecart_type(t1))
        None
        '''
        # numpy.nanstd permet d'ignorer les valeurs manquantes
        # mais s'il n'y en a pas, elle ne fonctionne pas!
        if self.nom_variable in table.header:
            try : ecart_type = numpy.nanstd(table.get_donnees(self.nom_variable))
            except : ecart_type = numpy.std(table.get_donnees(self.nom_variable))
            return ecart_type

    def normaliser(self, table):
        '''
        Permet de normaliser les données de la variable contenue dans la table

        Attributs
        ---------
        table : Table
        tableau de données contenant les données à normaliser

        Return
        --------
        value: type
        description

        Examples
        --------
        >>> t1=Table(["var1","var2"],\
            numpy.array([[0,1],[2,6],[3,7],[4,8],[5,10]],dtype=float))
        >>> t2=Table(["var1","var2","var3"],\
            numpy.array([[0,1,2],[1,2,3],[3,4,5],[4,5,12],[5,6,20]],dtype=float))
        >>> n1=Normalisation("var1")
        >>> n2=Normalisation("var3")
        >>> print(n1.normaliser(t1).body)
        [[-1.62746694  1.        ]
         [-0.46499055  6.        ]
         [ 0.11624764  7.        ]
         [ 0.69748583  8.        ]
         [ 1.27872403 10.        ]]
        >>> print(n2.normaliser(t1).body)
        La variable var3 n'existe pas dans la table sélectionnée.
        [[-1.62746694  1.        ]
         [-0.46499055  6.        ]
         [ 0.11624764  7.        ]
         [ 0.69748583  8.        ]
         [ 1.27872403 10.        ]]
        >>> print(n2.normaliser(t2).body)
        [[ 0.          1.         -0.94527391]
         [ 1.          2.         -0.79757486]
         [ 3.          4.         -0.50217676]
         [ 4.          5.          0.53171657]
         [ 5.          6.          1.71330895]]
        '''
        # Création d'une nouvelle table
        new_table = Table(table.header, table.body)
        
        # Permet de vérifier si la variable existe dans la table
        # Puis remplace les données par leur valeur normalisée
        try:
            moyenne = Centrage(self.nom_variable).calcul_moyenne(table)
            std = self.calcul_ecart_type(table)
            index_variable = table.get_index_var(self.nom_variable)
            for i in range(0, len(table.body[:, index_variable])):
                new_table.body[i, index_variable] = (
                    table.body[i, index_variable] - moyenne)/std
            return new_table
        except:
            print("L'opération de normalisation n'a pas fonctionné.")
            return table

    def __str__(self):
        return "Je vais normaliser les données de la variable : " +\
            self.nom_variable + " contenue dans une table."


if __name__ == '__main__':
    doctest.testmod()
