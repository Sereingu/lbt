'''
Script contenant la classe Operation_mathematique
'''

from ptd.transform.transformation import Transformation


class Operation_mathematique(Transformation):
    '''
    Classe mère de toutes les opérations mathématiques (à effectuer sur les données) implémentées

    Attributs
    ---------
    nom_variable : str
    nom de la variable sur laquelle les opérations doivent être effectuées
    '''

    def __init__(self, nom_variable):
        self.nom_variable = nom_variable
