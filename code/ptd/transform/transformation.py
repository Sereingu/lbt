'''
Script contenant la classe Transformation
'''
from abc import ABC

class Transformation(ABC):
    '''
    Classe vide, parente de plusieurs classes, notamment :
    Operation_mathematique, Fusion, Filtrage
    Elles-mêmes parentes d'autres classes.
    '''
